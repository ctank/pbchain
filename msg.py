""" msg.py: Creates new message definitions from JSON files 

This module parses JSON files that specify the formats for each message. 
It will also allow you to create new messages for enqueing into the 
central dispatch queue. 

Copyright (C) 2020 Colin Tan
"""

import logging
import json

class Messages:
    """ Create a new messages class, loading the message definitions from the message file """
    def parseCommentedJsonFile(self, file):
        """ Parses a JSON file with comments.

        A comment must be preceded by a '#', which must come AFTER
        the comma that separates fields, as everything after the '#' is ignored
        including all commas.

        Params:
            file: Commented JSON file to read.

        Returns:
            dict containing fields in file.
        """

        try:
            cleanedLines = ''

            with open(file) as f:
                while True:
                    line = f.readline()

                    if line:
                        tok = line.split('#')[0].strip()

                        if len(tok) > 0:
                            cleanedLines = cleanedLines + tok + '\n'
                    else:
                        break;

                output = json.loads(cleanedLines)

                return output

        except json.decoder.JSONDecodeError as e:
            print("\n\n***Failed to parse JSON:\n\n%s" % cleanedLines)
            print(e)
            print("\n\n")
            return None
        except:
            raise
                    
    def __init__(self, message_file):
        """ Constructor: 

        Params: 
        message_file: File that contains the JSON definition for each message type.

        Returns: None

        """

        self.directory = None
        self.messages = self.parseCommentedJsonFile(message_file)

    def append(self, newfile):
        """ Appends messages in newfile to the current set of messages.

        Params:
            newfile: JSON file containing new message definitions

        Returns: None

        """

        newmessages = self.parseCommentedJsonFile(newfile)
        self.messages = {**self.messages, **newmessages}

    def __make_directory(self):
        """ Private method: Make message directory.

        Creates a message directory consisting of the tag name and
        description, IF the 'Description', 'desc' or 'description'
        fields are present in the definition. Otherwise there will be
        an empty string.
        
        Params: None
        Returns: None

        """

        # Construct the directory

        self.directory = []

        for key in self.messages.keys():
            desc = self.get_desc(key)

            self.directory.append((key, desc))

    def get_directory(self):
        """ Get message directory.

            Returns the directory, which is a list of the form (tag, description). The "description"
            may be empty.

            Params: None
            Returns: Message directory.
        """

        if self.directory is None:
            self.__make_directory()

        return self.directory

    def search_directory(self, str):
        """ Search message directory.

        This does a substring search of the message descriptions. and returns a list of tags that
        match.

        Params:
        str: String to searc

        Returns:
        A list of tags.
        """

        if str == '':
            return []

        if self.directory is None:
            self.__make_directory()

        result = []
        for dir in self.directory:
            if dir[1].find(str) != -1:
                result.append(dir[0])

        return result

    def make_message(self, tag, data=''):
        """ Extracts a message definition and optionally sets its fields.

        Params:
        tag: Tag for message type to extract.
        data: If specified, the fields that are in data and in the message are set to
                the values given.

        Returns:
        An instance of the message type, with the fields set according to data, if specified.
        """

        if self.messages is None:
            return None

        if tag in self.messages:
            msg = dict(self.messages[tag])

            if isinstance(data, dict):
                for key in list(data.keys()):
                    if key in msg:
                        msg[key] = data[key]

            return msg
        else:
            return None


    def get_desc(self, tag):
        """ Get message type description

        Returns description for the message type specified by tag.

        Params:
        tag: Short string identifying the message type, as specified in the JSON file.

        Returns:
        The description for the message type.
        """

        if self.messages is None:
            return None

        if tag in self.messages:
            desc = ''
            msg = self.messages[tag]

            if 'Description' in msg:
                desc = msg['Description']

            if 'desc' in msg:
                desc = msg['desc']

            if 'description' in msg:
                desc = msg['description']

            if 'Desc' in msg:
                desc = msg['Desc']

            
            return desc

        else:
            return "No such tag: " + tag
