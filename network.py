import json

"""
Base class for network connections.
"""

class NetworkBase:
    def __init__(self, blockchain, connect_string = ''):
        """ Initialize the network class.

        Params:
            blockchain = Blockchain object.
            connect_string = Connection string, useful only for systems
            with a centralized node.

        Returns: None
        """

        self.connect_string = connect_string
        self.bc = blockchain

    def handle_peer_add_request(self, request):
        """ Add a new peer node.

        Params:
            request: Peer add request. Must have the form
            {"host":<hostname>,
            "port":<portnum>,
            "netid":<netid>
            }

        Returns:None
        """

        return self.bc.add_peer(request)

    def handle_block_broadcast(self, block):
        """ Handles a block broadcast from another peer

        block = Blockchain block
        Returns: None
        """

        ret =  self.bc.add_block_from_other_peers(block)

        if ret is not None:
            return "Block Add Success"
        else:
            return None

    def handle_peer_transaction(self, sender_id, receiver_id, transaction):
        """ Handle transaction broadcast from another peer
        
        Params:
            sender_id, receiver_id: IDs 
            transaction: Transaction to be broadcast
        """

        if self.bc.send_new_transaction(sender_id, receiver_id, transaction, broadcast = False):
            return "Transaction Add Success"
        else:
            return None

    def handle_new_transaction(self, sender_id, receiver_id, transaction):
        """ Handle transaction broadcast from another peer
        
        Params:
            sender_id, receiver_id: IDs 
            transaction: Transaction to be broadcast
        """

        if self.bc.send_new_transaction(sender_id, receiver_id, transaction, broadcast = True):
            return "Success"
        else:
            return None

    def handle_get_blockchain_len(self):
        """ Handle blockchain length request.

        Params: none
        Returns: Blockchain length
        """

        return self.bc.get_blockchain_len()

    def handle_get_blockchain(self, index):
        """ Get blockchain

        Param:
            index: Get blockchain from this block onwards
        Returns:
            Blockchain from index provided onwards
        """

        return self.bc.get_blockchain(index)

    def handle_get_peers(self):
        """ Gets all this blockchain's peers

        Params: None
        Returns: A list of peers of the form:
        {"host":<hostname>,
        "port":<portnum>
        }
        """

        return self.bc.get_peers()

    def handle_add_local_transaction(self, sender_id, receiver_id, transaction):
        """ PUBLIC API: Add a new transactions
        """

        self.bc.send_new_transaction(sender_id, receiver_id, transaction)

    def handle_get_confirmed_transactions(self, index):
        return self.bc.get_confirmed_transactions(index)

    def handle_get_unconfirmed_transactions(self):
        """ Handle request for unhandled transactions
        """

        return self.bc.get_unconfirmed_transactions()

    def handle_mine_request(self):
        """ Handle mining requests
        """

        ret =  self.bc.mine_block()

        if ret is not None:
            return ret.__dict__

    def handle_get_coin_balances(self, ID):
        """ Get coin balances
        """

        coin = self.bc.get_coin()

        if coin is not None:
            return coin.get_balance(ID)

    def handle_get_coin_hold_balances(self, ID):
        """ Get hold balances
        """

        coin = self.bc.get_coin()

        if coin is not None:
            return coin.get_hold_balance(ID)

    def handle_get_unconfirmed_coin_transactions(self, ID):
        """ Get the list of unconfirmed coin transactions
        """

        coin = self.bc.get_coin()

        if coin is not None:
            return coin.get_transaction(ID)

    def handle_get_hold_coin_transactions(self, ID):
        """ Get the list of coin transactions on hold
        """

        coin = self.bc.get_coin()

        if coin is not None:
            return coin.get_hold_transaction(ID)

    def handle_get_confirmed_coin_transactions(self, ID):
        """ Get the list of confirmed coin transactions
        """

        coin =  self.bc.get_coin()

        if coin is not None:
            return coin.get_confirmed_transaction(ID)

    ###
    ### Following functions shpuld be overriden to make the respective
    ### requests with peers.
    ###


    def broadcast_block(self, peers, block):
        """ OVerride this to broadcast newly mined blocks
        """
        pass

    def broadcast_transaction(self, peers, sender_id, receiver_id, transaction):
        """ Override this to broadcast new transactions
        """

        pass

    def request_chain_len(self, peer):
        """ Request chain length from a peer
        """

        pass

    def broadcast_request_chain_len(self, peers):
        """ Broadcast chain length request to all peers
        """

        pass

    def download_chain(self, peer, start_index):
        """ Download chains from peers
        """
        pass

    def download_transactions(self, peer, start_index):
        """ Download transactions from a peer
        """

        pass

    def register_self_to_peer(self, peer):
        """ Register self to a peer
        """

        pass
