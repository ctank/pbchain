""" Helper methods to produce PBCoin messages """
import msg
import chain_utils
import requests
import json

messages = msg.Messages('pbcoin.json')
headers = {'Content-type':'application/json'}

ref = ''

def pay_pbcoin(from_id, to_id, amount, key):
    coin = messages.make_message('pay', {'from': from_id, 'to': to_id, 'amount':amount})
    return chain_utils.make_transaction(coin, from_id, to_id, key)

def mint_pbcoin(to_id, key):
    coin = messages.make_message('mint', {'from':to_id,'to':to_id})
    return chain_utils.make_transaction(coin, to_id, to_id, key)

def make_ref(server):
    if not server.startswith('http://'):
        server = 'http://' + server

    return server + '/add_new_transaction'
        
def set_server(server):
    global ref

    ref = make_ref(server)

def pay(from_user, to_id, amount):
    global ref

    if ref == '':
        print("Call set_server to set server first.")
        return

    coin = pay_pbcoin(from_user['ID'], to_id, amount, from_user['key'])
    print("Sending: ", coin)
    requests.post(ref, headers = headers, data = json.dumps(coin))

