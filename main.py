from flask import Flask
import pbchain
import requests
from flask import request, render_template
import json
import sys
import time

app = Flask(__name__)

pb_network = None

def outcome(ret):
    if ret is not None:
        return json.dumps(ret, indent=4, separators=(',', ': ')), 200
    else:
        return "Error.", 500

@app.route('/', methods = ['GET'])
def root_fn():
    ret =  {"title":"PBChain", "version":"0.1"}
    return outcome(ret)


@app.route('/notify_new_block', methods = ['POST'])
def notify_new_block():
    block = request.get_json()
    ret =  pb_network.handle_block_broadcast(block)
    return outcome(ret)


@app.route('/notify_new_transaction', methods = ['POST'])
def notify_new_transaction():
    trans_obj = request.get_json()
    ret =  pb_network.handle_peer_transaction(trans_obj['sender_id'],
    trans_obj['receiver_id'], trans_obj['transaction'])
    return outcome(ret)

@app.route('/get_chain_len', methods = ['GET'])
def get_chainlen():
    retlen = pb_network.handle_get_blockchain_len()

    ret = {'chain_len': retlen}
    return outcome(ret)

@app.route('/get_chain', methods = ['GET'])
def get_chain():

    indexstr = request.args.get('index')

    if indexstr is not None:   
        index = int(indexstr)
    else:
        index = 0

    if index is not None:
        blockchain =  pb_network.handle_get_blockchain(index)

    if blockchain is not None:
        ret = []
        for block in blockchain:
            ret.append(block.__dict__)
    else:
        return "Invalid chain.", 500
    return outcome(ret)

@app.route('/register_peer', methods = ['POST'])
def register_peer():
    req = request.get_json()
    print("\n\nREGISTER PEER", req)
    ret = pb_network.handle_peer_add_request(req)
    return outcome(ret)

@app.route('/get_peers', methods = ['GET'])
def get_peers():
    retlist =  pb_network.handle_get_peers()
    ret = {"peers":retlist}
    return outcome(ret)

@app.route('/add_new_transaction', methods = ['POST'])
def add_new_transaction():
    trans = request.get_json()

    if trans:
        ret =  pb_network.handle_new_transaction(trans['senderID'],
        trans['receiverID'], trans)

        return outcome(ret)
    else:
        return "Bad transaction.", 500

@app.route('/get_confirmed_transactions', methods = ['GET'])
def get_confirmed_transactions():

    ndx_string = request.args.get('index')

    if ndx_string is not None:
        index = int(ndx_string)
    else:
        index = 0

    ret =  pb_network.handle_get_confirmed_transactions(index)
    return outcome(ret)

@app.route('/get_unconfirmed_transactions', methods = ['GET'])
def get_unconfirmed_transactions():
    ret =  pb_network.handle_get_unconfirmed_transactions()
    return outcome(ret)

@app.route('/mine', methods = ['GET'])
def mine():
    ret = pb_network.handle_mine_request()
    return outcome(ret)

@app.route('/get_coin_balances', methods = ['GET'])
def get_balances():
    ID = request.args.get('ID')
    ret = pb_network.handle_get_coin_balances(ID)
    return outcome(ret)

@app.route('/get_unposted_coin_transactions', methods = ['GET'])
def get_unposted_transactions():
    ID = request.args.get('ID')
    ret = pb_network.handle_get_unconfirmed_coin_transactions(ID)
    return outcome(ret)

@app.route('/get_hold_coin_transactions', methods = ['GET'])
def get_hold_transactions():
    ID = request.args.get('ID')
    ret = pb_network.handle_get_hold_coin_transactions(ID)
    return outcome(ret)

@app.route('/get_posted_coin_transactions', methods = ['GET'])
def get_posted_transactions():
    ID = request.args.get('ID')
    ret = pb_network.handle_get_confirmed_coin_transactions(ID)
    return outcome(ret)

@app.route('/get_coin_hold_balances', methods = ['GET'])
def get_hold_balance():
    ID = request.args.get('ID')
    ret = pb_network.handle_get_coin_hold_balances(ID)
    return outcome(ret)

@app.route('/info', methods = ['GET'])
def show():
    uri = 'http://127.0.0.1:' + str(pb_port)

    coin_balances = requests.get(uri + '/get_coin_balances')
    coin_hold_balances = requests.get(uri + '/get_coin_hold_balances')
    unc_trans = requests.get(uri + '/get_unconfirmed_transactions')

    if coin_balances.status_code == 200:
        balances = coin_balances.json()
    else:
        balances = {'error':coin_balances.status_code}

    if coin_hold_balances.status_code == 200:
        hold_balances = coin_hold_balances.json()
    else:
        hold_balances = {'error':coin_hold_balances.status_code}

    if unc_trans.status_code == 200:
        unc_transactions = unc_trans.json()
    else:
        unc_transactions = {'error':unc_trans.status_code}

    info = {'balances':balances, 'hold_balances':hold_balances, 'unconfirmed':unc_transactions}

    return render_template('info.html', host = pb_host + ':' + str(pb_port), 
    info = info)

if len(sys.argv) != 3:
    print("\nUsage: %s <config filename> <username>\n\n" % sys.argv[0])
    exit(-1)

if __name__ == '__main__':
    pbc = pbchain.PBChain(pb_conf = sys.argv[1], user_conf = sys.argv[2])
    (pb_network, pb_host, pb_port) = pbc.get_network()

    print("Starting blockchain")
    pbc.start()

    print("Starting API")
    app.run(port = pb_port)
