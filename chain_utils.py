""" This contains utility functions for the blockchain, like
generating wallet addresses from public keys, etc."""

from Crypto.Hash.SHA256 import SHA256Hash
from Crypto.Hash.RIPEMD import RIPEMD160Hash
from base58check import b58encode
import ecdsa
import binascii
import json
import time

def get_wallet_ID(pubkey):
    """ Creates the wallet ID from the provided public key,
    following these steps:

       i) Get SHA256 hash
       ii) Apply RIPE160 hash
       iii) Append '00' to front of hash
       iv) Apply SHA256 twice to iii)
       v) Extract first four bytes (8 charactesr) from iv), append
           to back of iii)
       vi) Apply Base58

    Params:
       pubkey: Public key, as an ASCII hex string
       Returns: Wallet ID, as an ASCII string

    """

    # Compute SHA256
    pkey = binascii.unhexlify(pubkey)
    pubkey_256 = SHA256Hash(pkey).digest()
    pubkey_ripe = RIPEMD160Hash(pubkey_256).hexdigest()
    pubkey_ripe = '00' + pubkey_ripe
    pubkey_ripe_2561 = SHA256Hash(binascii.unhexlify(pubkey_ripe)).digest()
    pubkey_ripe_2562 = SHA256Hash(pubkey_ripe_2561).hexdigest()
    pubkey_ripe = pubkey_ripe + pubkey_ripe_2562[:8]

    return b58encode(binascii.unhexlify(pubkey_ripe)).decode()

def verify_wallet_ID(pubkey, address):
    """ Check that the address provided matches the public key. This
    is so that you cannot sign for someone else using your own keypair.

        Params:
            pubkey: Public key in ASCII hex form.
            address: Wallet address in ASCII form.
    """

    verify_address = get_wallet_ID(pubkey)

    return address == verify_address

def make_keypair(filename = None):
    """ Generate signing and verification keys.

    Params:
        filename: Name of file to write public (.pub) and private
        (.prv) keys.

    Returns:
        Dictionary structure of form:
        {
            "private":<Private key string>,
            "public":<Public key string>
        }

        Keys are returned as hex ASCII strings.
    """

    sk = ecdsa.SigningKey.generate(curve = ecdsa.SECP256k1)
    prvkey = sk.to_string().hex()
    pubkey = sk.verifying_key.to_string().hex()
    
    if filename is not None:
        pubkeyfname = filename + '.pub'
        prvkeyfname = filename + '.prv'

        with open(pubkeyfname, "w") as f:
            f.write(pubkey)

        with open(prvkeyfname, "w") as f:
            f.write(prvkey)

    return {"private":prvkey, "public":pubkey}

def make_private_key(prv_key_string):
    """ Generate a private (signing) key.

    Params:
        prv_key_string:
            Private key string, ASCII Hex

       Returns:
            A signing key object.
   """

    pkey = binascii.unhexlify(prv_key_string)
    return ecdsa.SigningKey.from_string(pkey,
    curve = ecdsa.SECP256k1)

def make_public_key(pub_key_string):
    """ Generate a public (verifying) key.

    Params:
        pub_key_string:
            Public key string, ASCII Hex

       Returns:
            A verifying key object.
   """

    pkey = binascii.unhexlify(pub_key_string)
    return ecdsa.VerifyingKey.from_string(pkey,
    curve = ecdsa.SECP256k1)

def load_private_key_string(prv_filename):
    """ Get the private key as a string from the
    given file.

    Params:
        prv_filename: Name of file storing private key
    Returns:
        Private key as an ASCII hex string
    """

    with open(prv_filename) as f:
        data  = f.read()
    return data

def load_private_key(prv_filename):
    """ Create a new private key object from
    a private key string in prv_filename.

    Params:
        prv_filename: Name of file containing private key as a hex string.
    Returns:
        A private key object that can be used to sign.
    """

    data = load_private_key_string(prv_filename)
    return make_private_key(data)

def load_public_key_string(pub_filename):
    """ Get the public key as a string from the
    given file.

    Params:
        pub_filename: Name of file storing public key
    Returns:
        Public  key as an ASCII hex string
    """

    with open(pub_filename) as f:
        data = f.read()

    return data

def load_public_key(pub_filename):
    """ Create a new public key object from
    a public key string in prv_filename.

    Params:
        pub_filename: Name of file containing public key as a hex string.
    Returns:
        A public key object that can be used to verify signatures.
    """

    data = load_private_key_string(pub_filename)
    return make_public_key(data)

def sign_message(message, private_key_string, public_key_string):
    """ Sign a message with the given private key.
    Returns a signature with the public key appended to it.
        
        Params:
            message: Message to be signed. Must be a sequence of bytes.
            private_key_string: ASCII hex string of the private key.
            public_key_string: ASCII hex string of the public key.

        Returns:
            An ASCII string of the signature (first 64 bytes) followed by
            the public key (remaining bytes)
    """

    sk = make_private_key(private_key_string)
    sig = sk.sign(message)
    return sig.hex() + public_key_string

def get_public_key_from_signature(signature):
    """ Extract the public key from the signature block.

        Params:
            signature: Signature block created by sign_message,
            consisting of the sigatnature itself and the public
            key needed to verify it.

        Returns:
            The public key embedded in signature.
    """

    sig_bin = binascii.unhexlify(signature)
    return sig_bin[64:].hex()

def get_signature_from_signature(signature):
    """ Extracts the signature portion of a signature block

    Params:
        signature: The signature block consisting of the
        signature itself and the public key to verify it.

    Returns:
        The signature itself.
    """
        
    sig_bin = binascii.unhexlify(signature)
    return sig_bin[:64].hex()

def verify_signature(signature, message):
    """ Verify signature blocks.

    Params:
        signature: Signature block consisting of signature and
        public key.

    Returns:
        True if the signature can be verified by the enclosed
        public key. False otherwise.
    """

    
    try:
        sig_part = get_signature_from_signature(signature)
        pub_key = get_public_key_from_signature(signature)
        pubkey = make_public_key(pub_key)

        pubkey.verify(binascii.unhexlify(sig_part), message)
        print("Signature passed.")
        return True
    except ecdsa.BadSignatureError:
        print("Signature failed.")
        return False
    except:
        print("Error verifying signature.")
        return False


def make_transaction(transaction, sender_ID, receiver_ID, key):
    """ Create a new blockchain transaction, with signature.

        Params:
            transaction: Transaction to be enclosed. It must be json
            serializable.
            sender_ID: Wallet ID for the sender. It must match
            the public key or the transaction will be rejected.
            receiver_ID: Wallet ID for the receiver.
            prv_key_string: Private key hex string to sign the message.
            pub_key_string: Public key hex string to attach to the
            signature to form the signature block.

        Returns: A transaction of the form:
            {
                "senderID": <Sender's ID>,
                "receiverID": <Receiver's ID>,
                "timestamp": <UNIX style timestamp>,
                "transaction": <The transaction itself>,
                "signature": <The signature block>
            }
    """

    priv_key_string = key['private']
    pub_key_string = key['public']

    if not verify_wallet_ID(pub_key_string, sender_ID):
        print("%s does not own provided public key." % sender_ID)
        return None
    else:
        ret =  {"senderID": sender_ID, 
        "receiverID":receiver_ID,
        "timestamp":time.time(),
        "transaction":transaction}

        sig = sign_message(json.dumps(ret, sort_keys = True).encode(), priv_key_string,
        pub_key_string)

        ret['signature'] = sig

        return ret

def verify_transaction(transaction, check_signature = True):
    """ Verify a transaction as valid.

    This function checks the structure of the transaction, ensures
    that the public key and senderID match, and verifies the
    signature. 

    Note that this function DOES NOT check if the transaction can
    actually go through, e.g. if there is enough balance. This must be
    done separately.

    Params:
        transaction: The transaction to verify.

    Returns:
        True if transaction verifies, False if a field
        is missing, the senderID does not match the public key,
        or the signature test fails.
    """

    # Check that all needed fields are present.
    if 'senderID' not in transaction\
    or 'receiverID' not in transaction\
    or 'timestamp' not in transaction\
    or 'signature' not in transaction:
        print("Invalid transaction")
        return False

    # Get signature
    tmp = dict(transaction)
    sig = tmp['signature']

    # Delete this key
    del(tmp['signature'])

    pubkey = get_public_key_from_signature(sig)

    if not verify_wallet_ID(pubkey, tmp['senderID']):
        print("Sender ID %s does not match public key." % 
        tmp['senderID'])
        return False

    if check_signature:
        if not verify_signature(sig, json.dumps(tmp, sort_keys = True).encode()):
            print("Signature check failed.")
            return False
        else:
            return True
    else:
        return True

def make_new_user(username):
    """ Creates a new user wallet and stores it into <username>.user
        Wallet contains user ID, public key and private key
    """

    keypair = make_keypair()
    walletID = get_wallet_ID(keypair['public'])

    user_dict = {'ID':walletID,
    'pubkey': keypair['public'],
    'prvkey': keypair['private']
    }

    fname = username + '.user'
    print("Writing to %s." % fname)
    with open(fname, "w") as f:
        json.dump(user_dict, f, indent = 4)

    return user_dict

def load_user(username):
    """ Loads the user wallet, which contains the ID and public and private keys.
    """

    fname = username + '.user'

    try:
        with open(fname) as f:
            user_dict = json.load(f)

        return {
        "ID":user_dict['ID'],
        "key":
        {"private":user_dict['prvkey'],
        "public":user_dict['pubkey']}
        }
    except:
        print("Unable to open user file %s." % fname)
        return None

def dict_to_bytes(the_dict):
    return json.dumps(the_dict, sort_keys = True).encode()

