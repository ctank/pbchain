""" userstore.py

This namespace defines the UserStore object to store user variables and messages
"""

from pymongo import MongoClient
import chain_utils
import PBRestrictedPython
import threading
from multiprocessing import Process
import marshal
import binascii
import json
import time
import constants

# SET TO FALSE TO DISABLE CONFIRMATION HOOKS
DEBUG = True

#DEBUG = False


class UserStore:
    def __init__(self, connect_string = 'mongodb://localhost:27017', max_exec_time = 2.0):

        """ Initialize the user store. We also have a max_exec_time parameter
        to specify the maximum running time of an agent's code. Default is 2 seconds
        """

        self.client = MongoClient(connect_string)
        self.db = self.client['userstore']
        self.msg = self.db['messages']
        self.obj = self.db['objects']
        self.max_exec_time = max_exec_time

        # Connection to the blockchain
        self.blockchain = None

        # Add self to the safe_builtins so the code can access
        # the user store
        # PBRestrictedPython.safe_builtins['db'] = self
        PBRestrictedPython.safe_builtins['agent_id'] = None
        PBRestrictedPython.safe_builtins['msg_id'] = None

    def set_blockchain(self, blockchain):
        """ Sets the blockchain """

        self.blockchain = blockchain

    def __dict_to_bytes__(self, the_dict):
        return json.dumps(the_dict, sort_keys = True).encode()


    def sign_message(self, message, creator_id, key):
        """ Sign a message """
        
        if not chain_utils.verify_wallet_ID(key['public'], creator_id):
            print("SIGN FAIL: ID %s DOES NOT OWN KEY." 
            % (creator_id))

            message['signature'] = 'INVALID SIGNATURE. DO NOT PASS.'
            return message

        message['signature'] = ''
        msgbytes = self.__dict_to_bytes__(message)
        sig = chain_utils.sign_message(msgbytes, key['private'],
        key['public'])

        message['signature'] = sig
        return message

    def __test_fields__(self, packet, fields):
        for field in fields:
            if field not in packet:
                return False

        return True

    def __test_sig_id__(self, creator_id, signature, agent_request):
        """ Verify signature and ID """

        # Verify the signature
        # Duplicate the agent request object
        test_obj = dict(agent_request)

        # Now remove the signature
        test_obj['signature'] = ''

        test_obj_bytes = self.__dict_to_bytes__(test_obj)

        # Test the signature
        if not chain_utils.verify_signature(signature, test_obj_bytes):
            return False
        else:
            pubkey = chain_utils.get_public_key_from_signature(signature)
            return chain_utils.verify_wallet_ID(pubkey, creator_id)
        

    def add_agent(self, agent_request):
        """ Add a new agent to the database
        """

        #### TODO: Look at how to implement payment for agent execution ###

        ret = False

        if self.__test_fields__(agent_request, ['type', 'agent_id', 'agent_code', 'creator_id', 
        'signature', 'max_exec_time']):
            # Check the creator data
            if self.__test_sig_id__(agent_request['creator_id'], 
            agent_request['signature'],
            agent_request):
                query = {"agent_id":agent_request['agent_id']}

                # Duplicate the agent request
                data = dict(agent_request)

                # Then delete the signature since we no longer need it
                data.pop('signature', None)

                # Check if this agent_id already exists
                if self.obj.count_documents(query) == 0:
                    self.obj.insert_one(data)
                    ret = True
                else:
                    # Get the record
                    agent = self.obj.find_one(query)

                    if agent['creator_id'] == agent_request['creator_id']:
                        self.obj.update_one(query, {'$set': data})
                        ret = True
                    else:
                        print("Creator %s does not own agent %s." %
                        (agent_request['creator_id'], agent_request['agent_id']))
                        ret = False
        else:
            print("Invalid agent packet.")
            ret = False

        return ret
    
    def get_agent(self, agent_ID):
        """ Get agent with ID
        """

        ret = None

        query = {'agent_id':agent_ID}

        if self.obj.count_documents(query) == 0:
            print("Agent %s does not exist." % agent_ID)
            ret = None
        else:
            ret = self.obj.find_one(query, {'_id':0})

        return ret

    def __check_agent__(self, agent_id):
        query = {'agent_id': agent_id}
        agent = None

        if self.obj.count_documents(query) > 0:
            # Yes it is an agent. Load the agent
            agent = self.obj.find_one(query)

        return agent

    def recv_trans(self, trans_request):
        """ Receive a list of transactions for the agent
        """

        ret = False

        if self.__test_fields__(trans_request, ['type', 'recv_id', 'creator_id',
        'signature', 'phase', 'trans_list']):
            
            if self.__test_sig_id__(trans_request['creator_id'],
            trans_request['signature'], trans_request):

                agent = self.__check_agent__(trans_request['recv_id'])

                if agent is not None:
                    self.__execute_agent__({'code':agent['agent_code'],
                    'max_exec_time':agent['max_exec_time'],
                    'agent_id':trans_request['recv_id'],
                    'message_id':None,
                    'trans_list':trans_request['trans_list']})

                    ret = True

                else:
                    print("Agent does not exist.")
            else:
                print("Transaction signature fail")
        else:
            print("Malformed transaction request.")
        
        return ret

    def recv_message(self, message_request):
        """ Receive a new message. If meant for an agent, it will trigger the execution
        of the agent."""

        ret = False
        if self.__test_fields__(message_request, ['type', 'recv_id', 'creator_id', 'signature',
        'message', 'message_id']):

            # Check signature and ID
            if self.__test_sig_id__(message_request['creator_id'],
            message_request['signature'], message_request):
                print("Signature passed. Retrieving agent.")

                msg = {'recv_id': message_request['recv_id'], 
                'message': message_request['message'], 
                'message_id':message_request['message_id'],
                'read':False}

                # Check whether recv_id is an existing agent
                agent = self.__check_agent__(message_request['recv_id'])

                if agent is not None:

                    print("YES we have agent!")

                    # Create the message
                    self.msg.insert_one(msg)

                    print("EXECUTING AGENT CODE.")

                    # Execute the agent's code
                    self.__execute_agent__({'code':agent['agent_code'], 
                    'max_exec_time':agent['max_exec_time'],
                    'agent_id': msg['recv_id'],
                    'message_id': msg['message_id'],
                    'trans_list': []})

                    ret = True
                else:
                    # Check whether this is a registered user
                    query = {'user_id': message_request['recv_id']}

                    if self.obj.count_documents(query) > 0:
                        # Yes, it is an agent. Create the message
                        self.msg.insert_one(msg)

                        ret = True
                    else:
                        print("Message addressed to non-existent user %s." % message_request['recv_id'])
                        ret = False
            else:
                print("Invalid signature.")
                ret = False
        else:
            print("Invalid message packet.")
            ret = False

        return ret
            
        pass

    def __execute_agent_code__(self, eggcode, agent_id, msg_id = None, trans_list = None):
        """ Executes the agent's code in a restricted environment """

        try:
            agent_builtins = dict(PBRestrictedPython.safe_builtins)

            agent_builtins['read_message'] = self.make_read_function(agent_id)
            agent_builtins['new'] = self.make_new(agent_id)
            agent_builtins['get'] = self.make_get(agent_id)
            agent_builtins['set'] = self.make_set(agent_id)

            agent_builtins['global_new'] = self.make_global_new(agent_id)
            agent_builtins['global_get'] = self.make_global_get(agent_id)
            agent_builtins['global_set'] = self.make_global_set(agent_id)

            if DEBUG is True:
                agent_builtins['execute_new'] = self.make_execute_new(agent_id)
                agent_builtins['execute_set'] = self.make_execute_set(agent_id)

            agent_builtins['agent_id'] = agent_id
            agent_builtins['msg_id'] = msg_id
            agent_builtins['trans_list'] = trans_list

            exec(eggcode, {'__builtins__': agent_builtins})
        except Exception as e:
            print("User code crashed. Error: ", e)
        
    def __make_agent_code_process__(self, eggcode, max_time, agent_id, msg_id = None,
    trans_list = None):
        """ Creates the process that runs the code. Kills process
        if it exceeds max_time seconds """

        # Create a new process to run the hexcode
        print("GOING TO EXECUTE.")
        pr = Process(target = self.__execute_agent_code__, args = (eggcode, 
        agent_id, msg_id, trans_list))
        print("GOING TO START.")
        pr.start()

        # Give it max_time seconds to run
        pr.join(max_time)

        # Kill the process if it is still alive
        if pr.is_alive():
            pr.kill()
            print("KILLED.")
        else:
            print("EXITED.")

    def __execute_agent__(self, execute_data):        
        """ Execute agent code """

        if self.__test_fields__(execute_data, ['code', 'max_exec_time',
        'agent_id', 'message_id', 'trans_list']):
            # Extract the code
            hexcode = execute_data['code']
            unhexcode = binascii.unhexlify(hexcode)
            eggcode = marshal.loads(unhexcode)

            # Check the run time
            max_exec_time = execute_data['max_exec_time']

            max_time = self.max_exec_time if self.max_exec_time < max_exec_time else max_exec_time

            # Get the agent ID and message ID
            agent_id = execute_data['agent_id']
            msg_id = execute_data['message_id']
            trans_list = execute_data['trans_list']

            th = threading.Thread(target = self.__make_agent_code_process__, 
            args = (eggcode, max_time, agent_id, msg_id, trans_list))

            th.start()
        else:
            print("Malformed agent execution packet.")

    def make_read_function(self, recv_id):
        def read_message(msg_id):
            read_request = {'type':2,
            'recv_id': recv_id,
            'msg_id': msg_id}

            return self.__read_message__(read_request)

        return read_message
        

    def __read_message__(self, read_request):
        """ Retrieves message """

        ret = None

        if self.__test_fields__(read_request, ['type', 'recv_id', 'msg_id']):
            # Check if signature and ID match
            # Extract the next unread message

            query = {'recv_id':read_request['recv_id']}

            if read_request['msg_id'] is None:
                query['read'] = False
            else:
                query['message_id'] = read_request['msg_id']

            if self.msg.count_documents(query) > 0:
                msg = self.msg.find_one(query)
                msg['read'] = True
                self.msg.update_one({'recv_id': msg['recv_id'],
                'message_id':msg['message_id']},
                {'$set': msg})
                ret =  msg['message']
            else:
                print("No unread messages.")
                ret = None
        else:
            print("Malformed read request.")
            ret = None

        return ret
                    
    def make_new(self, my_id):
        """ Return the function to create a new variable
        for this agent."""

        def new(var_name, init_value):
            create_data = {'creator_id': my_id,
            'var_name': var_name,
            'init_value': init_value}

            return self.create_variable(create_data)

        return new

    def create_variable(self, create_data, globalVar = False):
        """ Create a new variable """

        ret = False

        if self.__test_fields__(create_data, ['creator_id', 'var_name', 'init_value']):
            # Check signature
            cid = create_data['creator_id']

            query = {
            'var_name':create_data['var_name']}

            if self.db[cid].count_documents(query) > 0:
                # Variable already exists, call set_variable function
                # to set the variable's value
                # We never change the scope of a variable, hence
                # we do not change the "global" field.

                set_packet = {'creator_id': cid,
                'var_name':create_data['var_name'],
                'value':create_data['init_value']}

                print("\n\nCALLING SET VARIABLE.\n\n")
                self.set_variable(set_packet)

                ret = True
            else:
                # Create the variable
                var = {'creator_id':create_data['creator_id'],
                'var_name':create_data['var_name'],
                'value': create_data['init_value'],
                'global': globalVar}

                self.db[cid].insert_one(var)
                ret = True
        else:
            print("Malformed variable creation packet.")
            ret = False

        return ret

    def make_get(self, my_id):
        """ Return the get_variable function for this agent."""

        def get(var_name):
            get_data = {'creator_id': my_id,
            'var_name': var_name}

            return self.get_variable(get_data)

        return get

    def get_variable_data(self, get_data):
        """ Get variable value """

        ret = None
        if self.__test_fields__(get_data, ['creator_id', 'var_name']):
            cid = get_data['creator_id']
            query = {'var_name': get_data['var_name']}

            if self.db[cid].count_documents(query) > 0:
                var = self.db[cid].find_one(query)
                ret = var
            else:
                print("No such variable %s for agent %s." % (query['var_name'],
                cid))

                ret = None
        else:
            print("Malformed get packet.")
            ret = None

        return ret

    def get_variable(self, get_data):
        """ Get variable value """

        ret = self.get_variable_data(get_data)

        if ret is not None:
            ret = ret['value']

        return ret

    def make_set(self, my_id):
        """ Return the set variable function for this agent"""

        def set(var_name, value):
            set_data = {'creator_id': my_id,
            'var_name': var_name,
            'value': value}

            return self.set_variable(set_data)

        return set

    def set_variable(self, set_data):
        """ Set variable value """

        ret = False
        if self.__test_fields__(set_data, ['creator_id', 'var_name', 'value']):

            cid = set_data['creator_id']

            query = {'var_name': set_data['var_name']}

            if self.db[cid].count_documents(query) > 0:
                var = self.db[cid].update_one(query, {'$set':{'value': set_data['value']}})
                ret = True
            else:
                print("No such variable %s for agent %s." % (query['var_name'],
                cid))

                ret = False
        else:
            print("Malformed set packet.")
            ret = False

        return ret

    def publish_var_create(self, create_data):
        """ Publish creation of a new global variable
        """

        if self.blockchain is None:
            print("Blockchain must be set.")
            return False

        trans = {'from': create_data['creator_id'],
        'to': create_data['creator_id'],
        'command':constants.CREATE_VAR,
        'signature':'',
        'var_name':create_data['var_name'],
        'value':create_data['init_value']}

        return self.blockchain.send_new_transaction(trans['from'],
        trans['to'], trans, broadcast = False, check_signature = False)

    def make_global_new(self, my_id):
        """ Return function to create new global variable
        """

        def global_new(var_name, init_value):
            create_data = {'creator_id':my_id,
            'var_name':var_name,
            'init_value':init_value}

            return self.global_create_variable(create_data)

        return global_new

    # In our language when we create a variable
    # it only creates a variable creation transaction
    def global_create_variable(self, create_data):
        """ Create a new global variable 
        """

        # Check whether this variable exists

        var_data = self.get_variable_data({'creator_id':create_data['creator_id'],
        'var_name':create_data['var_name']})

        if var_data:
            if 'global' not in var_data or var_data['global'] is False:
                print("Existing variable %s declared as local. Fail."
                % (create_data['var_name']))
                return False

        print("PUBLISHING BABY!")
        ret = self.publish_var_create(create_data)

        print("POST PUBLISHING RET: ", ret)
        return ret

    def make_execute_new(self, agent_id):
        """ Create execute new method """

        def execute_new(var_name, value):
            trans = {'from': agent_id,
            'command':constants.CREATE_VAR,
            'var_name': var_name,
            'value': value}

            return self.execute_global_create_variable(trans)

        return execute_new


    # The actual variable is only created when the transaction is mined
    # and this function is called

    def execute_global_create_variable(self, create_trans):
        """ Called when "create variable" transaction is mined
        """

        create_data = {'type':2, 
        'creator_id':create_trans['from'],
        'var_name':create_trans['var_name'],
        'init_value':create_trans['value']}

        return self.create_variable(create_data, globalVar = True)


    def make_global_get(self, my_id):
        """ Return global version of get"""

        def global_get(var_name):
            get_data = {'creator_id': my_id,
            'var_name': var_name}

            return self.global_get_variable(get_data)

        return global_get

    def global_get_variable(self, get_data):
        """ Global version of get_variable. 
        """

        var = self.get_variable_data(get_data)

        if var is None:
            print("Variable %s does not exist." % (get_data['var_name']))
            return None

        if 'global' not in var or var['global'] is False:
            print("Variable %s is local." % (var['var_name']))
            return None
        else:
            return var['value']

    def publish_var_update(self, set_data):
        """ Publish update to variable
        """

        if self.blockchain is None:
            print("Blockchain must be set.")
            return False

        trans = {'from': set_data['creator_id'],
        'to': set_data['creator_id'],
        'command':constants.UPDATE_VAR, 
        'signature':'',
        'var_name':set_data['var_name'],
        'value':set_data['value']}

        return self.blockchain.send_new_transaction(trans['from'],
        trans['to'], trans, broadcast = False, check_signature = False)

    def make_global_set(self, my_id):
        """ Global version of set
        """

        def global_set(var_name, value):
            set_data = {'creator_id': my_id,
            'var_name': var_name,
            'value': value}

            return self.global_set_variable(set_data)

        return global_set

    def test_var_is_global(self, set_trans):
        get_data = {'creator_id':set_trans['creator_id'],
        'var_name':set_trans['var_name']}

        var_data = self.get_variable_data(get_data)

        if var_data is not None and 'global' in var_data:
            return var_data['global'] is True
        else:
            return False

    def make_execute_set(self, agent_id):
        """ Create an execute set function """

        def execute_set(var_name, value):

            trans = {'from':agent_id,
            'command':constants.UPDATE_VAR,
            'var_name':var_name,
            'value':value}

            return self.execute_global_set_variable(trans)

        return execute_set

    def execute_global_set_variable(self, set_trans):
        """ Execute setting a variable. Called when a "set_variable"
        transaction is mined.
        """
        set_data = {'creator_id': set_trans['from'],
        'var_name':set_trans['var_name'],
        'value':set_trans['value']}

        ret = False

        # Check that this is a global variable
        if self.__test_fields__(set_data, ['creator_id', 'var_name', 'value']):
            if self.test_var_is_global({"creator_id":set_trans['from'],
            'var_name':set_trans['var_name']}):
                ret = self.set_variable(set_data)
            else:
                print("Incorrect variable scope for %s." % set_data['var_name'])
                ret = False
        else:
            print("Malformed set packet.")
            ret = False

        return ret

    def global_set_variable(self, set_data):
        """ Global version of set_variable. Publishes
        value change
        """

        if self.test_var_is_global({'creator_id':set_data['creator_id'],
        'var_name':set_data['var_name']}):
            self.publish_var_update(set_data)
            return True
        else:
            print("Var does not exist or is local.")
            return False


