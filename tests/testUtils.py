import unittest
from unittest.mock import MagicMock
import time
import chain_utils

class testUtils(unittest.TestCase):
    def setUp(self):
        self.keys = chain_utils.make_keypair("test1")
        self.pubkey = self.keys['public']
        self.prvkey = self.keys['private']
        self.wallet1 = chain_utils.get_wallet_ID(self.pubkey)

        self.keys2 = chain_utils.make_keypair("test2")
        self.pubkey2 = self.keys2['public']
        self.wallet2 = chain_utils.get_wallet_ID(self.pubkey2)

    def test_verify_wallet_ID(self):
        print("Verify test wallet ID")
        self.assertEqual(chain_utils.verify_wallet_ID(self.pubkey,
        self.wallet1), True)

        self.assertEqual(chain_utils.verify_wallet_ID(self.pubkey,
        self.wallet2), False)

    def test_verify_transactions(self):

        print("Test verify transaction")

        testtrans = {"cmd":"take", "amt":50}
        transaction = chain_utils.make_transaction(testtrans,
        self.wallet1, self.wallet2, self.keys)

        old_trans = dict(transaction)

        # Verify that this transaction works
        self.assertEqual(chain_utils.verify_transaction(transaction), True)

        # Verify that the transaction wasn't altered by the verify
        # function
        self.assertEqual(transaction, old_trans)

        # Tamper with the transaction
        transaction['transaction']['amt'] = 70
        self.assertEqual(chain_utils.verify_transaction(transaction),
        False)

        # Revert back to old transaction
        transaction = dict(old_trans)
        transaction['timestamp'] = time.time()
        self.assertEqual(chain_utils.verify_transaction(transaction),
        False)

