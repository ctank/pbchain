import unittest
from unittest.mock import MagicMock
import block
import blockchain
import storage
import chain_utils
import json
import network

class testBlockchain(unittest.TestCase):
    def verify_hook(self, transaction):
        print("Wallet 1: %3.2f." % self.sim_balances[self.wallet1])

        amt = transaction['amount']
        from_field = transaction['from']
        to_field = transaction['to']

        balance = self.sim_balances[from_field] - amt

        print("Balance for %s: $%3.2f." % (from_field,
        self.sim_balances[from_field]))

        print("Amount to deduct: $%3.2f." % amt)
        print("Amount left: $%3.2f." % balance)

        if balance >= 0.0:
            self.sim_balances[from_field] = balance

        return balance >= 0.0

    def mine_hook(self, block, trans_list):
        # Start applying the transactions in the list
        for trans in trans_list:
            if "cmd" in trans:
                if trans["cmd"] == "pay":
                    amt = trans['amount']
                    from_addr = trans['from']
                    to_addr = trans['to']

                    print("Deducting $%3.2f from %s" % (amt, from_addr))
                    print("Adding to %s" % (to_addr))

                    self.balances[from_addr] = self.balances[from_addr] - amt
                    self.balances[to_addr] = self.balances[to_addr] + amt
                    self.sim_balances = dict(self.balances)

        print("\n\n*** Balances: ", self.balances, "***\n\n")

        return True

    def setUp(self):
        self.db  = storage.Storage()
        self.bc = blockchain.Blockchain()
        self.network = network.NetworkBase(blockchain = self.bc)
        self.bc.set_network(self.network)
        self.bc.set_storage(self.db)
        self.bc.verify_transaction_hook = MagicMock(
        side_effect = self.verify_hook)
        self.bc.new_transaction_mined_hook = MagicMock(
        side_effect = self.mine_hook)

        # Create cryptographic keys and wallet IDs
        self.keys1 = chain_utils.make_keypair()
        self.keys2 = chain_utils.make_keypair()
        self.wallet1 = chain_utils.get_wallet_ID(self.keys1['public'])
        self.wallet2 = chain_utils.get_wallet_ID(self.keys2['public'])

        self.balances = {}
        self.balances[self.wallet1] = 50
        self.balances[self.wallet2] = 5

        self.sim_balances = dict(self.balances)

        # Create test transactions
        self.trans1 = {"cmd":"pay", "from":self.wallet1, "to":self.wallet2,
        "amount":30}

        self.trans2 = {"cmd":"pay", "from":self.wallet2, "to":self.wallet1,
        "amount":40}

        self.trans3 = {"cmd":"revoke", "from":self.wallet1, "to":self.wallet1,
        "amount":0.0}

        self.trans4 = {"cmd":"pay", "from":self.wallet2, "to":self.wallet1,
        "amount":20}

    def make_transactions(self):
        print("\nNORMAL TEST\n")
        # Create transactions
        trans1 = chain_utils.make_transaction(self.trans1,
        self.wallet1, self.wallet2, self.keys1)

        trans2 = chain_utils.make_transaction(self.trans2,
        self.wallet2, self.wallet1, self.keys2)

        trans3 = chain_utils.make_transaction(self.trans3,
        self.wallet1, self.wallet1, self.keys1)

        trans4 = chain_utils.make_transaction(self.trans4,
        self.wallet2, self.wallet1, self.keys2)

        return trans1, trans2, trans3, trans4

    def test_blockchain_normal(self):

        self.bc.flush_block_chain()

        # Ensure that it is empty
        chain1 = self.db.get_chain(1)
        chain2 = self.db.get_chain(2)
        unconfirmed_transactions = self.db.get_trans_list(0)
        confirmed_transactions = self.db.get_trans_list(1)

        self.assertEqual(chain1, [])
        self.assertEqual(chain2, [])
        self.assertEqual(unconfirmed_transactions, [])
        self.assertEqual(confirmed_transactions, [])

        gen = self.bc.make_genesis_block()

        chain1 = self.db.get_chain(1)

        self.assertEqual(len(chain1), 1)
        the_block = chain1[0]
        self.assertEqual(the_block.index, 0)
        self.assertEqual(the_block.data_hash, 0)
        self.assertEqual(the_block.prev_hash, 0)

        # Create transactions
        (trans1, trans2, trans3, trans4) = self.make_transactions()

        # Add the transactions
        self.bc.send_new_transaction(self.wallet1, self.wallet2, trans1)

        unconfirmed_transactions = self.db.get_trans_list(0)
        self.assertEqual(len(unconfirmed_transactions), 1)
        self.assertEqual(unconfirmed_transactions[0], trans1)

        # Add in an illegal transaction
        self.bc.send_new_transaction(self.wallet2, self.wallet1, trans2)

        # Our number of transactions should remain unchanged
        unconfirmed_transactions = self.db.get_trans_list(0)
        self.assertEqual(len(unconfirmed_transactions), 1)
        assert(trans2 not in unconfirmed_transactions)

        # Now we mine the block
        block1 = self.bc.mine_block()

        # At the end, wallet1 will have 50 - 30 = 20, wallet 2 = 5 + 30 = 35
        self.assertEqual(self.balances[self.wallet1], 20)
        self.assertEqual(self.balances[self.wallet2], 35)

        chain = self.db.get_blockchain()
        unconfirmed_transactions = self.db.get_trans_list(0)
        confirmed_transactions = self.db.get_trans_list(1)

        self.assertEqual(chain, [gen, block1])
        self.assertEqual(unconfirmed_transactions, [])
        self.assertEqual(confirmed_transactions, [{"blocknum":1, "transactions":trans1}])

        # Let's do that again but with two transactions

        # Add the transactions
        self.bc.send_new_transaction(self.wallet1, self.wallet2, trans1)
        self.bc.send_new_transaction(self.wallet1, self.wallet1, trans3)
        self.bc.send_new_transaction(self.wallet1, self.wallet1, trans3)
        self.bc.send_new_transaction(self.wallet2, self.wallet1, trans4)

        # Mine
        block2 = self.bc.mine_block()
        unconfirmed_transactions = self.db.get_trans_list(0)
        confirmed_transactions = self.db.get_trans_list(1)
        self.assertEqual(unconfirmed_transactions, [])
        self.assertEqual(confirmed_transactions, 
        [trans1, trans3, trans3, trans4])

        chain = self.db.get_blockchain()

        self.assertEqual(chain, [gen, block1, block2])

        # Now the balances are:
        # wallet1  = 20 + 20 = 40, wallet2 = 35 - 20 = 15
        self.assertEqual(self.balances[self.wallet1], 40)
        self.assertEqual(self.balances[self.wallet2], 15)

        # No transactions, should be None
        block3 = self.bc.mine_block()
        unconfirmed_transactions = self.db.get_trans_list(0)
        self.assertEqual(unconfirmed_transactions, [])
        
        chain = self.db.get_blockchain()
        self.assertEqual(chain, [gen, block1, block2])

        # Balances remain the same
        # wallet1  = 40, wallet2 = 15
        self.assertEqual(self.balances[self.wallet1], 40)
        self.assertEqual(self.balances[self.wallet2], 15)

        # Get blockchain length
        print("Length: %d." % self.bc.get_blockchain_len())

        # Print the blockchain
        print("Chain: ", self.bc.get_blockchain(0))

if __name__ == '__main__':
    print("\n\n\n================== START =====================\n\n\n")
    unittest.main()

