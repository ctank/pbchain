import unittest
from unittest.mock import MagicMock
import block
import blockchain
import storage
import chain_utils
import json
import network

class testBlockchain(unittest.TestCase):
    def setUp(self):
        self.db  = storage.Storage()
        self.bc = blockchain.Blockchain()
        self.bc.set_storage(self.db)
        self.network = network.NetworkBase(self.bc)
        self.bc.set_network(self.network)

        # Create cryptographic keys and wallet IDs
        self.keys1 = chain_utils.make_keypair()
        self.keys2 = chain_utils.make_keypair()
        self.wallet1 = chain_utils.get_wallet_ID(self.keys1['public'])
        self.wallet2 = chain_utils.get_wallet_ID(self.keys2['public'])

        # Create test transactions
        self.trans1 = {"cmd":"pay", "from":self.wallet1, "to":self.wallet2,
        "amount":30}

        self.trans2 = {"cmd":"pay", "from":self.wallet2, "to":self.wallet1,
        "amount":40}

        self.trans3 = {"cmd":"revoke", "from":self.wallet1, "to":self.wallet1,
        "amount":0.0}

        self.trans4 = {"cmd":"pay", "from":self.wallet2, "to":self.wallet1,
        "amount":20}

    def make_transactions(self):
        # Create transactions
        trans1 = chain_utils.make_transaction(self.trans1,
        self.wallet1, self.wallet2, self.keys1)

        trans2 = chain_utils.make_transaction(self.trans2,
        self.wallet2, self.wallet1, self.keys2)

        trans3 = chain_utils.make_transaction(self.trans3,
        self.wallet1, self.wallet1, self.keys1)

        trans4 = chain_utils.make_transaction(self.trans4,
        self.wallet2, self.wallet1, self.keys2)

        return trans1, trans2, trans3, trans4

    def make_child_block(self, myblock, trans_list):
        """ Make children of the provided block
        """

        theblock = myblock['block']
        new_block = block.Block(index = theblock.index + 1,
        data_hash = theblock.data_hash, prev_hash = theblock.hash,
        difficulty = theblock.difficulty)

        return ({"trans":trans_list, "block":new_block}, json.dumps({"block":new_block.__dict__,
        "trans_list":trans_list}, sort_keys = True))

    def get_chains(self):
        chain1 = self.db.get_chain(1)
        chain2 = self.db.get_chain(2)

        return (chain1, chain2)

    def test_blockchain_fork(self):
        """ Test what hapens when we have a fork """


        print("\nTESTING FORKS\n")

        self.bc.flush_block_chain()

        # Ensure that it is empty
        (chain1, chain2) = self.get_chains()
        unconfirmed_transactions = self.db.get_trans_list(0)
        confirmed_transactions = self.db.get_trans_list(1)

        self.assertEqual(chain1, [])
        self.assertEqual(chain2, [])
        self.assertEqual(unconfirmed_transactions, [])
        self.assertEqual(confirmed_transactions, [])

        # Make the genesis block
        gen = self.bc.make_genesis_block()

        # Make the transactions
        (trans1, trans2, trans3, trans4) = self.make_transactions()

        # Add trans3 and make a block
        self.bc.send_new_transaction(self.wallet1, self.wallet2,
        trans3)

        # Mine the block
        block1 = self.bc.mine_block()
        self.assertEqual(self.db.fork, False)

#        # Force a start_fork to see if it works right
#        self.db.start_fork(block1.prev_hash)
#
#       self.assertEqual(self.db.fork, True)
#        (chain1, chain2) = self.get_chains()
#        self.assertEqual(chain1, [gen, block1])
#        self.assertEqual(chain2, [gen])

        # Create an identical block, but with its own hash
        newblock = block.Block(index = block1.index, data_hash = block1.data_hash,
        prev_hash = block1.prev_hash, difficulty = block1.difficulty)

        # Now attempt to add it
        in_block = {"block": newblock.__dict__,
        "trans_list": [trans3]}

        block2 = self.bc.add_block_from_other_peers(in_block)

        # A fork should have taken place
        self.assertEqual(self.db.fork, True)

        # Now both chain1 and chain2 will have the same blocks
        (chain1, chain2) = self.get_chains()
        self.assertEqual(chain1, [gen, block1])
        self.assertEqual(chain2, [gen, block2])

        # Do replay attack
        print("\n**** REPLAY ***\n\n")
        self.bc.add_block_from_other_peers(in_block)

        # Chain should remain the same
        (chain1, chain2) = self.get_chains()
        self.assertEqual(chain1, [gen, block1])
        self.assertEqual(chain2, [gen, block2])

        # Make child blocks. These are already in the correct
        # JSON format needed
        block1trans = {"trans":[trans3], "block":block1}
        (block11, block11json) = self.make_child_block(block1trans, [trans3])
        (block12, block12json) = self.make_child_block(block11, [trans3])
        block2trans = {"trans":[trans3], "block":block2}
        (block21, block21json) = self.make_child_block(block2trans, [trans3])
        (block22, block22json) = self.make_child_block(block21, [trans3])
        (block23, block23json) = self.make_child_block(block22, [trans3])
        (block24, block24json) = self.make_child_block(block23, [trans3])
        (block25, block25json) = self.make_child_block(block24, [trans3])
        (block26, block26json) = self.make_child_block(block25, [trans3])
    
        # Add block
        nblock11 = self.bc.add_block_from_other_peers(block11)
        (chain1, chain2) = self.get_chains()
        self.assertEqual(chain1, [gen, block1, nblock11])
        self.assertEqual(block11.__dict__, nblock11.__dict__)
        self.assertEqual(chain2, [gen, block2])

        nblock21 = self.bc.add_block_from_other_peers(block21)
        (chain1, chain2) = self.get_chains()
        self.assertEqual(chain2, [gen, block2, nblock21])
        self.assertEqual(block21.__dict__, nblock21.__dict__)
        self.assertEqual(chain1, [gen, block1, nblock11])

        nblock12 = self.bc.add_block_from_other_peers(block12)
        (chain1, chain2) = self.get_chains()
        self.assertEqual(chain1, [gen, block1, nblock11, nblock12])
        self.assertEqual(block12.__dict__, nblock12.__dict__)
        self.assertEqual(chain2, [gen, block2, nblock21])

        # Now add all the fork2 blocks
        nblock22 = self.bc.add_block_from_other_peers(block22)
        (chain1, chain2) = self.get_chains()
        self.assertEqual(chain2, [gen, block2, nblock21, nblock22])
        self.assertEqual(block22.__dict__, nblock22.__dict__)
        self.assertEqual(chain1, [gen, block1, nblock11, nblock12])

        # Now add all the fork2 blocks
        nblock23 = self.bc.add_block_from_other_peers(block23)
        (chain1, chain2) = self.get_chains()
        self.assertEqual(chain2, [gen, block2, nblock21, nblock22, nblock23])
        self.assertEqual(block23.__dict__, nblock23.__dict__)
        self.assertEqual(chain1, [gen, block1, nblock11, nblock12])

        nblock24 = self.bc.add_block_from_other_peers(block24)
        (chain1, chain2) = self.get_chains()
        self.assertEqual(chain2, [gen, block2, nblock21, nblock22, nblock23, nblock24])
        self.assertEqual(block24.__dict__, nblock24.__dict__)
        self.assertEqual(chain1, [gen, block1, nblock11, nblock12])

        nblock25 = self.bc.add_block_from_other_peers(block25)
        (chain1, chain2) = self.get_chains()
        self.assertEqual(chain2, [gen, block2, nblock21, nblock22, nblock23, nblock24, nblock25])
        self.assertEqual(block25.__dict__, nblock25.__dict__)

        # Fork should be resolved by now and chain1  is empty
        self.assertEqual(chain1, [])

        # Now chain 2 is 3 blocks longer than chain 1. This should resolve the fork
        self.assertEqual(self.db.fork, False)
        self.assertEqual(self.db.chain_num, 2)

        # Add our final block
        nblock26 = self.bc.add_block_from_other_peers(block26)
        (chain1, chain2) = self.get_chains()
        self.assertEqual(chain2, [gen, block2, nblock21, nblock22, nblock23, nblock24, nblock25,
        nblock26])
        self.assertEqual(block26.__dict__, nblock26.__dict__)
        self.assertEqual(chain1, [])

if __name__ == '__main__':
    print("\n\n\n================== START =====================\n\n\n")
    unittest.main()

