""" Test the HTTPNet class.
"""

import unittest
import  httpnet
from unittest.mock import MagicMock
import requests

class result:
    def __init__(self, status_code):
        self.status_code = status_code

    def json(self):
        return '[{"name":"test1", "val":1}, {"name":"test2", "val":2}]'

def sim_get(url):
    return result()

def sim_post(url, headers, data):
    return result()

requests.get = MagicMock(side_effect = sim_get)
requests.post = MagicMock(side_effect = sim_post)

class TestHTTPNet(unittest.TestCase):
    def setUp(self):
        self.network = httpnet.HTTPNet(None)

    def test_bad_peer(self):
        peer = {'a':'b', 'c':'d'}
        x = self.network.request_chain_len(peer)
        self.assertEqual(x, None)

if __name__ == '__main__':
    unittest.main()
