import unittest
import accounts
import chain_utils

class testAccounts(unittest.TestCase):
    """ Test the accounts class """

    def setUp(self):
        self.accounts = accounts.Accounts()
        self.user1 = chain_utils.load_user('polarbear')
        self.user2 = chain_utils.load_user('kucinta')

        self.user1ID = self.user1['ID']
        self.user2ID = self.user2['ID']

        # Initialize the coins in circulation (cic)

        self.accounts.init_cic()

        # Get balance
        cic_bal = self.accounts.get_balance("cic")
        self.assertEqual(cic_bal['balance'], self.accounts.cic)
        cic_hbal = self.accounts.get_hold_balance("cic")
        self.assertEqual(cic_hbal['hold_balance'], -1)

    def print_conf_trans(self):
        print("\nConfirmed transactions.")
        res = self.accounts.confirmed_transactions.find()

        for item in res:
            print(item)

    def test_accounts(self):
        # Let's mint a coin
        self.accounts.mint(self.user1ID, 12.5)
        self.accounts.mint(self.user2ID, 23.75)

        # Get transactions
        user1trans = self.accounts.get_transaction(self.user1ID)
        print("USER1: ", user1trans)

        print("\n\n++++++ SETTLING +++++++++++++=\n")
        # Do settlement
        self.accounts.settle(blocknum = 1)

        print("\n\n-------- EXTRACTING ---------\n")

        # Now get hold balances:
        cic_bal = self.accounts.get_balance('cic')
        user1_bal = self.accounts.get_balance(self.user1ID)
        user2_bal = self.accounts.get_balance(self.user2ID)

        self.assertEqual(cic_bal['balance'], self.accounts.cic)
        self.assertEqual(user1_bal['balance'], -1)
        self.assertEqual(user2_bal['balance'], -1)

        print("\n\nEXTRACTING HOLD BALANCES")

        # Test the hold balances

        cic_hbal = self.accounts.get_hold_balance('cic')
        user1_hbal = self.accounts.get_hold_balance(self.user1ID)
        user2_hbal = self.accounts.get_hold_balance(self.user2ID)

        self.assertEqual(cic_hbal['hold_balance'], self.accounts.cic - 12.5 - 23.75)
        self.assertEqual(user1_hbal['hold_balance'], 12.5)
        self.assertEqual(user2_hbal['hold_balance'], 23.75)

        # Now finalize. We used block 1.
        self.accounts.finalize(1)
        cic_bal = self.accounts.get_balance('cic')
        user1_bal = self.accounts.get_balance(self.user1ID)
        self.assertEqual(cic_bal['balance'], self.accounts.cic - 12.5 - 23.75)
        self.assertEqual(user1_bal['balance'], 12.5)

        print("\nCONFIRMED TRANSACTIONS\n")
        self.print_conf_trans()

        cic_bal = self.accounts.get_balance('cic')
        self.assertEqual(cic_bal['balance'], self.accounts.cic - 12.5 - 23.75)
        user2_bal = self.accounts.get_balance(self.user2ID)
        self.assertEqual(user2_bal['balance'], 23.75)

        print("\nCONFIRMED TRANSACTIONS\n")
        self.print_conf_trans()

        # Now transfer from user1 to user2
        self.accounts.transfer(from_id = self.user1ID, to_id = self.user2ID, 
        amount = 5, signature = 'sig')
        self.accounts.transfer(from_id = self.user2ID, to_id = self.user1ID, 
        amount = 10, signature = 'sig')
        self.accounts.settle(blocknum = 2)
        self.accounts.finalize(2)
    
        user1_bal = self.accounts.get_balance(self.user1ID)
        user2_bal = self.accounts.get_balance(self.user2ID)

        # user1: 12.5 - 5 + 10 = 17.5
        # user2: 23.75 + 5 - 10 = 18.75

        self.assertEqual(user1_bal['balance'], 17.5)
        self.assertEqual(user2_bal['balance'], 18.75)
        print("\nCONFIRMED TRANSACTIONS\n")
        self.print_conf_trans()

        self.accounts.transfer(from_id = self.user1ID, to_id = self.user2ID,
        amount = 20.0, signature = 'sig')

        self.accounts.transfer(from_id = self.user2ID, to_id = self.user1ID,
        amount = 10.75, signature = 'sig')

        self.accounts.transfer(from_id = self.user1ID, to_id = self.user2ID,
        amount = 3.00, signature = 'sig')

        self.accounts.settle(blocknum = 3)
        self.accounts.finalize(3)

        # user1 = 17.5 - 20 = fail
        # user2 = 18.75 - 10.75 = 8, user1 = 17.5 + 10.75 = 28.25
        # user1 = 28.25 - 3 = 25.25, user2 = 8 + 3 =11

        user1_bal = self.accounts.get_balance(self.user1ID)
        user2_bal = self.accounts.get_balance(self.user2ID)
        self.assertEqual(user1_bal['balance'], 25.25)
        self.assertEqual(user2_bal['balance'], 11)
        print("\nCONFIRMED TRANSACTIONS\n")
        self.print_conf_trans()

        
    def tearDown(self):
        self.accounts.wipe()

if __name__ == '__main__':
    unittest.main()

