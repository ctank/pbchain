import unittest
from unittest.mock import MagicMock
import user_store
import json
import chain_utils
import marshal
import blockchain

# Our test agent code
agent_code = """
        
print("My agent ID: ", agent_id)
print("Message ID: ", msg_id)

msg = read_message(msg_id)

print("NEW FUNCTION", new)

print("*** Message ", msg, "****")

print("Making a variable x")
new('x', 'Hello')
val = get('x')
print("Variable x value: ", val)
val = get('y')
print("Nonexistent value: ", val)
set('x', 15)
val = get('x')
print("New value: ", val)

new('x', 'Lalalala')
val = get('x')
print("New X: ", val)

i = 1
while i<10:
    print(i)
    i = i + 1
    time.sleep(1)

"""

agent2_code = """
print("**** AGENT2: Testing Global Variable access")

print("**** AGENT2: Making a new variable.")
res = global_new('x', 3.0)
print("**** AGENT2: res1: ", res)

print("**** AGENT2: Making another new variable.")
res = global_new('xxx', 12.0)
print("****AGENT2: res2: ", res)

print("**** AGENT2: Executing create 1")
res = execute_new('x', 3.0)
print("*** AGENT2: res3: ", res)

print("**** AGENT2: Executing create 2")
res = execute_new('xxx', 12.0)
print("*** AGENT2: res4: ", res)

print("**** AGENT2: Getting 1")
val = global_get('xxx')
print("*** AGENT2: xxx: ", val)

print("**** AGENT2: Getting 2")
val = global_get('x')
print("*** AGENT2: x: ", val)

print("**** AGENT2: SET 1")
res = global_set('x', 'Hello World')
print("*** AGENT2: res5: ", res)


print("**** AGENT2: Getting 3")
val = global_get('x')
print("*** AGENT2: x: ", val)

print("**** AGENT2: SET 2")
res = execute_set('x', 'Hello World')
print("*** AGENT2: res6: ", res)


print("**** AGENT2: Getting 4")
val = global_get('x')
print("*** AGENT2: x: ", val)
"""

# Side-effect for the send_new_transaction mock
def new_trans(from_data, to_data, trans, broadcast, check_signature):
    print("\nNew Transaction: ")
    print("From: ", from_data)
    print("To: ", to_data)
    print("Broadcast: ", broadcast)
    print("Check Signature: ", check_signature)
    print("Transaction: ", trans, "\n")

    return True

# Set up the mock
blockchain.Blockchain.send_new_transaction = MagicMock(side_effect = new_trans)

bc = blockchain.Blockchain()

class testUserStore(unittest.TestCase):

    def setUp(self):
        self.db = user_store.UserStore(max_exec_time = 400)
        self.db.set_blockchain(bc)
        self.user = chain_utils.load_user('kucinta')
        self.ID = self.user['ID']
        self.key = self.user['key']

        self.agent = chain_utils.load_user('lala')
        self.agent_ID = self.agent['ID']

        self.agent2 = chain_utils.load_user('polarbear')
        self.agent2_ID = self.agent2['ID']

    def tearDown(self):
        pass

    def compile(self, code_text):
        eggcode = compile(code_text, '<string>', mode = 'exec')
        rawcode = marshal.dumps(eggcode)
        return rawcode.hex()

    def make_agent(self, code_text, agent_id, creator_id, creator_key):
        hexcode = self.compile(code_text)

        agent = {
        'type':1,
        'agent_id': agent_id,
        'agent_code':hexcode,
        'creator_id':creator_id,
        'signature':'',
        'max_exec_time': 3.0
        }

        self.db.sign_message(agent, creator_id, creator_key)
        self.db.add_agent(agent)

        return agent
        
    def test_add_agent(self):

        print("\n\n================== ADD AGENT ===============\n\n")

        agent = self.make_agent(agent_code, self.agent_ID,
        self.ID, self.key)

        agent2 = self.db.get_agent(self.agent_ID)

        # Get rid of signature from agent to compare
        agent.pop('signature', None)
        self.assertEqual(agent, agent2)

    def test_message(self):

        print("\n\n================== TEST MESSAGE ====================\n\n")
        msg = {'type':2,
        'recv_id':self.agent_ID,
        'creator_id':self.ID,
        'signature':'',
        'message':'Boo',
        'message_id':'1'}

        self.db.sign_message(msg, self.ID, self.key)

        self.db.recv_message(msg)

    def test_variables(self):
        print("\n\n================= TEST LOCALS ===================\n\n")
        msg = {'type':2,
        'creator_id': self.ID,
        'var_name': 'x',
        'init_value':'baller'}

        self.db.create_variable(msg)

        msg2 = {'type':2,
        'creator_id': self.ID,
        'var_name':'x'}

        val = self.db.get_variable(msg2)
        self.assertEqual(val, 'baller')

        msg['init_value'] = 3
        self.db.create_variable(msg)

        val = self.db.get_variable(msg2)
        self.assertEqual(val, 3)

        msg2['var_name'] = 'z'
        val = self.db.get_variable(msg2)
        self.assertEqual(val, None)

        msg3 = {'type':3,
        'creator_id': self.ID,
        'var_name': 'x',
        'value': 'funk'}

        res = self.db.set_variable(msg3)
        self.assertEqual(res, True)

        msg2['var_name'] = 'x'
        val = self.db.get_variable(msg2)
        self.assertEqual(val, 'funk')

        msg3['var_name'] = 'z'
        res = self.db.set_variable(msg3)
        self.assertEqual(res, False)

    def test_global_variables(self):
        msg1 = {'type':2,
        'creator_id':self.agent_ID,
        'var_name':'x',
        'init_value':3.0}

        print("\n\n==================== GLOBALS ====================\n\n")
        print("\nTESTING CREATE GLOBAL\n")
        flag = self.db.global_create_variable(msg1)
        print("flag: ", flag, "\n")

        self.assertEqual(flag, False)

        print("\nTESTING CREATE GLOBAL\n")

        msg1['var_name'] = 'xxx'
        flag = self.db.global_create_variable(msg1)
        print("flag: ", flag, "\n")

        trans = {}
        trans['from'] = self.agent_ID
        trans['to'] = self.agent_ID
        trans['var_name'] = 'xxx'
        trans['value'] = 3.0

        flag = self.db.execute_global_create_variable(trans)

        self.assertEqual(flag, True)

        print("\n\nCREATE RESULT: ", flag, "\n\n")

        print("\nTESTING GET GLOBAL\n")
        val = self.db.global_get_variable(msg1)

        print("Val is ", val, "\n")

        self.assertEqual(val, 3.0)

        print("\nTESTING GET GLOBAL NON EXISTENT\n")
        msg1['var_name'] = 'kaput'
        val = self.db.global_get_variable(msg1)

        print("Val is ", val, "\n")
        self.assertEqual(val, None)

        print("\nTESTING GET GLOBAL LOCAL\n")
        msg1['var_name'] = 'x'
        val = self.db.global_get_variable(msg1)

        print("Val is ", val, "\n")
        self.assertEqual(val, None)

        print("\nTESTING SET GLOBAL VARIABLE LOCAL\n")
        msg2 = {'creator_id': self.agent_ID,
        'var_name':'x',
        'value':123}

        res = self.db.global_set_variable(msg2)
        print("\nres: ", res)
        self.assertEqual(res, False)

        print("\nTESTING SET GLOBAL VARIABLE NOT EXIST\n")
        msg2['var_name'] = 'yyy'
        res = self.db.global_set_variable(msg2)
        print("Res: ", res)
        self.assertEqual(res, False)

        print("\nTESTING SET GLOBAL VARIABLE GOOD\n")
        msg2['var_name'] = 'xxx'
        res = self.db.global_set_variable(msg2)
        print("Res: ", res)
        self.assertEqual(res, True)

        print("\nTESTING GET GLOBAL PRE EXECUTE\n")
        msg1['var_name'] = 'xxx'
        val = self.db.global_get_variable(msg1)

        print("Val is ", val, "\n")
        self.assertEqual(val, 3.0)

        # Do the execute
        print("\nTESTING SET GLOBAL EXECUTE\n")
        res = self.db.execute_global_set_variable(
        {"from":self.agent_ID,
        "var_name":"xxx",
        "value":msg2["value"]})

        self.assertEqual(res, True)

        print("\nTESTING GET GLOBAL POST EXECUTE\n")
        msg1['var_name'] = 'xxx'
        val = self.db.global_get_variable(msg1)

        print("Val is ", val, "\n")
        self.assertEqual(val, 123)

    def test_global_agent(self):

        # Create the agent
        print("\n\n=================== GLOBAL AGENT =================\n\n")
        agent2 = self.make_agent(agent2_code, self.agent2_ID,
        self.ID, self.key)

        # Senda  message to trigger the agent
        msg = {'type':2,
        'recv_id':self.agent2_ID,
        'creator_id':self.ID,
        'signature':'',
        'message':'Bah!',
        'message_id':'1'}

        print("\nEXECUTING AGENT 2\n")

        self.db.sign_message(msg, self.ID, self.key)
        self.db.recv_message(msg)
        print("\n")


if __name__ == '__main__':
    unittest.main()

