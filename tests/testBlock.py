""" Simple test to see that block creation works """

import unittest
import time
import block
import json
from Crypto.Hash.SHA256 import SHA256Hash

class testblock(unittest.TestCase):
    def test_block(self):
        print("Doing block creation test on block.py")
        gb = block.Block(-1, 'xxx', 'yyy', genesis = True)
        t1 = time.time()
        b1 = block.Block(0, 'ccc', 'ddd')
        t2 = time.time()
        b2 = block.Block(1, 'zzz', '111', 
        difficulty = 4)

        b1hash = b1.hash
        b2hash = b2.hash

        b1.hash = 0
        b2.hash = 0

        self.assertEqual(b1.index, 0)
        self.assertEqual(b1.data_hash, 'ccc')
        self.assertEqual(b1.prev_hash, 'ddd')
        assert(abs(b1.time_stamp - t1) < 0.5)
        
        newb1hash = SHA256Hash(json.dumps(b1.__dict__, sort_keys = True).encode()).hexdigest()
        b1.hash = newb1hash

        self.assertEqual(b1.hash, b1hash)
        assert(b1.hash.startswith('0' * b1.difficulty))


        self.assertEqual(b2.index, 1)
        self.assertEqual(b2.data_hash, 'zzz')
        self.assertEqual(b2.prev_hash, '111')
        assert(abs(b2.time_stamp - t2) < 0.5)
        
        newb2hash = SHA256Hash(json.dumps(b2.__dict__, sort_keys = True).encode()).hexdigest()
        b2.hash = newb2hash

        self.assertEqual(b2.hash, b2hash)
        assert(b2.hash.startswith('0' * b2.difficulty))

if __name__ == '__main__':
    unittest.main()
