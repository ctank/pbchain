import chain_utils

username = input("\nEnter your name: ")
userdata = chain_utils.make_new_user(username)

print("\nData generated for %s." % username)
print("------------------------------\n\n")

print("Your ID: %s" % userdata['ID'])
print("Your public key: %s" % userdata['pubkey'])
