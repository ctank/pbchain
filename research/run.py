import binascii
import marshal
import json
import sys
from RestrictedPython import safe_builtins, safe_builtins
import testclass
import builtins

if len(sys.argv) != 2:
    print("\nUsage: %s <json filename>\n" % sys.argv[0])
    exit(-1)

__metaclass__ = type
__name__ = '__main__'

with open(sys.argv[1]) as f:
    egg_dict = json.load(f)

eggunhex = binascii.unhexlify(egg_dict['payload'])
eggcode = marshal.loads(eggunhex)

#safe_builtins['input'] = input
#safe_builtins['print'] = print
#safe_builtins['testclass'] = testclass

exec(eggcode, {'__builtins__':safe_builtins})
#exec(eggcode)
