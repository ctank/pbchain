__builtins__['input'] = builtins.input
__builtins__['print'] = builtins.print
__builtins__['testclass'] = testclass

a1 = testclass.A(1)
a2 = testclass.A(2)

a1.print()
a2.print()
