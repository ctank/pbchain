import binascii
import marshal
import json
import sys
import py_compile

if len(sys.argv) != 2:
    print("\nUsage: %s <.py file>\n" % sys.argv[0])
    exit(-1)

fname = sys.argv[1]

# Split the filename to get rid of the .py
the_name = fname.split('.')[0]

# We are only interested in the first part of the filename. Now compile.

pyname = './' + the_name + '.py'
pycname = './' + the_name + '.pyc'
jsonfname = './' + the_name + '.json'

with open(pyname) as f:
    code_text = f.read()

# Let's compile it
code = compile(code_text, '<string>', mode = 'exec')

rawcode = marshal.dumps(code)

# Now write the dictionary
egg_dict = {'title':'pbchain code', 'payload':rawcode.hex()}

# Dump the JSON
print("Writing to %s." % jsonfname)
with open(jsonfname, "w") as f:
    json.dump(egg_dict, f)

