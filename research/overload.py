class Complex:
    def __init__(self, re, im):
        self.re = re
        self.im = im

    def __add__(self, other):
        return Complex(self.re + other.re, self.im + other.im)

    def __str__(self):
        return 'Re: %3.4f, Im: %3.4f' % (self.re, self.im)

    def __setattr__(self, name, value):
        print("name: %s, value: %s." % (name, value))
        if name == 're' or name == 'im':
            print("WHY YOU ANYHOW SET %s?" % name.upper())
            self.__dict__[name] = value

c = Complex(1, 2)
d = Complex(3, 4)
e = c + d

print(e)
