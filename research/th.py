import threading
import time

lock = threading.Lock()

def t1():
    time.sleep(2)
#    lock.acquire()
    print("Hello t1")
    time.sleep(5)
    print("Bye t1!")
#    lock.release()
    
def t2():
#    lock.acquire()
    print("Bye t2!")
    time.sleep(5)
    print("Hello t2!")
#    lock.release()
    
th1 = threading.Thread(target = t1)
th2 = threading.Thread(target = t2)

th1.start()
th2.start()

while(1):
    pass

