"""  block.py: This namespace contains the definition for a basic
    blockchain block """

from Crypto.Hash.SHA256 import SHA256Hash
import json
import time

print("CRAZY")

class Block:
    """ Basic blockchain block, consisting of the following fields:
      
        index: Unique index for this block.
        time_stamp: Time block was formed.
        data_hash: Hash for the data, from Merkle Tree
        prev_hash: Hash for previous block
        hash: Hash for this block.
        nonce: Nonce for the cryptographic challenge
    """

    def __init__(self, index, data_hash, prev_hash,
    difficulty = 3, genesis = False, do_hash = True):
        """ Intiailize the block with the given
        index, hash, previous hash, and difficulty level.
        If genesis is True this is generated as a genesis block.
        If do_hash is False, we suppress hash generation. Used mostly
        to convert blocks from other peers from JSON.
        """

        self.index = index
        self.time_stamp = time.time()
        self.data_hash = data_hash
        self.prev_hash = prev_hash
        self.difficulty = difficulty
        self.nonce = 0
        self.hash = 0

        if not genesis:
            if do_hash:
                self.hash = self.find_hash()
            else:
                print("Leaving hash empty.")
                self.hash = 0
        else:
            print("Genesis block.")
            self.hash = SHA256Hash(json.dumps(self.__dict__, sort_keys = True).encode()).hexdigest()

    def find_hash(self):
        """ Compute a hash with the suitable proof of work
        """

        challenge = '0' * self.difficulty
        hash = ''

        hash = SHA256Hash(json.dumps(self.__dict__, sort_keys = True).encode()).hexdigest()

        while not hash.startswith(challenge):
            self.nonce = self.nonce + 1
            hash = SHA256Hash(json.dumps(self.__dict__, sort_keys = True).encode()).hexdigest()

        return hash

    def __str__(self):
        return json.dumps(self.__dict__)
