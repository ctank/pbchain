""" This namespace maintains the accounts object that keeps track of pbcoins
that are in service."""

from pymongo import MongoClient
from pymongo.errors import BulkWriteError
import chain_utils

class Accounts:
    def __init__(self, connect_string = 'mongodb://localhost:27017/', database = 'accounts'):
        self.client = MongoClient(connect_string)
        self.db = self.client[database]

        # Holds ID and public key
        self.accounts = self.db['accounts']

        # Holds account balances
        self.balances = self.db['balances']

        # Holds account hold balances
        self.hold_balances = self.db['hold_balances']

        # Holds transactions
        self.transactions = self.db['transactions']

        # Holds holding transactions
        self.hold_transactions = self.db['hold_transactions']


        # Holds confirmed transactions
        self.confirmed_transactions = self.db['confirmed_transactions']

        # Number of coins in circulation
        self.cic = 500000000

        self.cic_user = chain_utils.load_user('cic')


    def init_cic(self):
        """ Initialize the coins in circulation. Can only be done once."""

        if self.check_balance_exists("cic"):
            print("Account exists.")
            return False

        cic_acct = {"ID":"cic", "balance":self.cic}
        self.balances.insert_one(cic_acct)

    def mint(self, to_id, amount):
        self.transfer(from_id = 'cic', to_id = to_id, amount = amount, 
        signature = "nosig")

    def check_balance_exists(self, ID):
        """ Check if we have a balance for ID"""

        return self.balances.count_documents({"ID":ID}) > 0

    def check_account_exists(self, ID):
        """ Check if account exists
            """

        return self.accounts.count_documents({"ID":ID}) > 0

    def get_balance(self, ID):
        """ Get account balance
        """

        if ID is None:
            ret = list(self.balances.find({}, {'_id':False}))
            return ret

        my_id = {"ID":ID}
        if self.balances.count_documents(my_id) > 0:
            res = self.balances.find_one(my_id, {'_id':False})
            return res
        else:
            print("No balance found for %s." % ID)
            return {"ID":ID, "balance": -1}

    def get_hold_balance(self, ID):
        """ Get hold balances """

        if ID is None:
            return list(self.hold_balances.find({}, {'_id':False}))

        my_id = {"ID":ID}

        if self.hold_balances.count_documents(my_id) > 0:
            res = self.hold_balances.find_one(my_id, {'_id':False})
            return res
        else:
            print("No hold balanaces found for %s." % ID)
            return {"ID":ID, "hold_balance": -1}

    def get_transaction(self, ID):
        """ Fetch transaction info for ID """

        if ID is None:
            ret = list(self.transactions.find({}, {'_id':False}))
            return ret

        query = {"ID":ID}
        if self.transactions.count_documents(query) == 0:
            print("No transactions for %s" % ID)
            return None

        trans = self.transactions.find_one(query, {'_id':False})
        return trans

    def get_hold_transaction(self, ID):
        """ Fetch hold transactions for ID"""

        if ID is None:
            return list(self.hold_transactions.find({}, {'_id':False}))

        query = {"ID":ID}

        if self.hold_transactions.count_documents(query) == 0:
            print("No hold transactions for %s" % ID)

        htrans = self.hold_transactions.find_one(query, {'_id':False})
        return htrans

    def get_confirmed_transaction(self, ID):
        """ Fetch hold transactions for ID"""

        if ID is None:
            return list(self.confirmed_transactions.find({}, {'_id':False}))

        query = {"ID":ID}

        if self.confirmed_transactions.count_documents(query) == 0:
            print("No confirmed  transactions for %s" % ID)

        ctrans = self.confirmed_transactions.find_one(query, {'_id':False})
        return ctrans

    def transfer(self, from_id, to_id, amount, signature):
        """ Transfer from from_id to to_id. Signature is used
        to search for this particular transaction
        """

        from_query = {"ID":from_id}
        to_query = {"ID":to_id}

        new_trans = {"ID":0, "debit":[], "credit":[], "nett":0.0}

        if self.transactions.count_documents(from_query) == 0:
            from_trans = dict(new_trans)
            from_trans['ID'] = from_id
            #print("NEW TRANS: ", from_trans)

            self.transactions.insert_one(from_trans)

            assert(self.transactions.count_documents({"ID":from_id})  == 1)

        if self.transactions.count_documents(to_query) == 0:
            to_trans = dict(new_trans)
            to_trans['ID'] = to_id
            #print("NEW TRANS: ", to_trans)
            self.transactions.insert_one(to_trans)
            assert(self.transactions.count_documents({"ID":to_id})  == 1)

        gv = self.transactions.find_one(from_query)
        
        #print("gv = ", gv)
        nett = gv['nett'] - amount

        # Get balances
        bal = self.get_balance(from_id)['balance']
        hbal = self.get_hold_balance(from_id)['hold_balance']

        # Find the smaller of the two

        if bal >= 0.0 and bal < hbal:
            cbal = bal
        elif hbal >= 0.0 and hbal < bal:
            cbal = hbal
        elif bal < 0.0:
            cbal = hbal
        else:
            cbal = bal

        #print("Settle Balance ", cbal)

        test_balance = cbal + nett
        #print("Test balance: %3.2f" % test_balance)

        if cbal + nett < 0:
            print("\n\n++++Insufficient balance for account %s.++++\n\n" % from_id)
            return False
        
        act_trans = {"blocknum":-1, "signature":signature, "amount":amount}

        # Balance is ok. Add this to the debit list
        gv['debit'].append(act_trans)
        gv['nett'] = nett

        #print("New gv ", gv)
        # Update the receiver
        recv = self.transactions.find_one(to_query)
        recv['credit'].append(act_trans)
        recv['nett'] = recv['nett'] + amount

        #print("recv ", recv)

        # Update
        self.transactions.update_one(from_query, {"$set":gv})
        self.transactions.update_one(to_query, {"$set":recv})

        return True

    def settle(self, blocknum):
        """ Settle all outstanding transactions, writing
        out hold_balances"""

        outstanding = self.transactions.find({'nett':{"$ne":0}})

        for trans in outstanding:
            query = {"ID":trans['ID']}
            nett = trans['nett']
            debits = trans['debit']
            credits = trans['credit']

            # Update the block numbers for all transactions
            for c in trans['credit']:
                c['blocknum'] = blocknum

            for d in trans['debit']:
                d['blocknum'] = blocknum
            
            # See if we have a hold balance
            hold = self.get_hold_balance(trans['ID'])

            if hold['hold_balance'] >= 0:
                # Yes. Update the hold balance
                hold['hold_balance'] = hold['hold_balance'] + nett

                # Save it
                self.hold_balances.update_one(query, {"$set":hold})
                #print("RECONCILE: Updated hold balance")

            else:
                # Get the balance
                bal = self.get_balance(trans['ID'])
                print("Creating hold balance for ", trans['ID'])

                if bal['balance'] >= 0:
                    nett_bal = bal['balance'] + nett
                else:  
                    nett_bal = nett

                # Save it as a hold balance
                hold_bal = {"ID":trans['ID'], "hold_balance": nett_bal}
                print("Adding new hold balance record ", hold_bal)

                self.hold_balances.insert_one(hold_bal)

                # See that the record is updated
                assert(self.hold_balances.count_documents({"ID":trans['ID']}) == 1)

                print("Record added.")
                print("RECONCILE: Created new hold balance for %s from balance."
                % trans['ID'])

            # Place the transaction in the hold list
            if self.hold_transactions.count_documents(query) == 0:
                self.hold_transactions.insert_one(trans)
            else:
                prev_trans = self.hold_transactions.find_one(query)
                #print("\n\nPREV TRANS:", prev_trans, "\n\n")

                total_debits = 0.0
                total_credits = 0.0
                for debit in trans['debit']:
                    total_debits = total_debits - debit['amount']
                    prev_trans['debit'].append(debit)

                for credit in trans['credit']:
                    total_credits = total_credits + credit['amount']
                    prev_trans['credit'].append(credit)

                nett = total_debits + total_credits

                prev_trans['nett'] = prev_trans['nett'] + nett

                #print("\n\nNEW TRANS: ", prev_trans, "\n\n")

                self.hold_transactions.update_one(query,
                {"$set":prev_trans})

            clear_trans = {"ID":trans['ID'], "debit":[], "credit":[], "nett":0.0}
            self.transactions.update_one(query, {"$set":clear_trans})

            x = self.get_transaction(trans['ID'])
            #print("UPDATED TRANSACTIONS: ", x)

            y = self.get_hold_transaction(trans['ID'])
            #print("Hold transaction: ",  y)

    def adjust_netts(self, debits, credits):
        
        #print("\nDEBITS!!")
        for debit in debits:
            debit_query = {"ID":debit['ID']}
            trans = self.get_hold_transaction(debit['ID'])

            if trans is not None:
                #print("BEFORE: ", trans)
                trans['nett'] = trans['nett'] + debit['details']['amount']

                new_debit = [x for x in trans['debit'] 
                if x != debit['details']]

                trans['debit'] = new_debit

                self.hold_transactions.update_one(debit_query, {"$set":trans})
                trans = self.get_hold_transaction(debit['ID'])
                #print("AFTER: ", trans)

                if self.confirmed_transactions.count_documents(debit_query)> 0:
                    debit_res = self.confirmed_transactions.find_one(debit_query)
                    debit_res['debit'].append(debit['details'])
                    debit_res['nett'] = debit_res['nett'] - debit['details']['amount']
                    self.confirmed_transactions.update_one(debit_query, {"$set":debit_res})
                else:
                    new_debit = {"ID":debit['ID'], 'credit':[], 'debit':[debit['details']], 'nett':-debit['details']['amount']}
                    self.confirmed_transactions.insert_one(new_debit)

        #print("\nCREDITS!!")
        for credit in credits:
            credit_query = {"ID":credit['ID']}

            trans = self.get_hold_transaction(credit['ID'])
            if trans is not None:
                #print("BEFORE: ", trans)
                trans['nett'] = trans['nett'] - credit['details']['amount']

                new_credit = [x for x in trans['credit']
                if x != credit['details']]

                trans['credit'] = new_credit

                self.hold_transactions.update_one(credit_query, {"$set":trans})
                trans = self.get_hold_transaction(credit['ID'])
                #print("AFTER: ", trans)

                if self.confirmed_transactions.count_documents(credit_query) > 0:
                    credit_res = self.confirmed_transactions.find_one(credit_query)
                    credit_res['credit'].append(credit['details'])
                    credit_res['nett'] = credit_res['nett'] + credit['details']['amount']
                    self.confirmed_transactions.update_one(credit_query, {"$set":credit_res})
                else:
                    new_credit = {"ID":credit['ID'], 'credit':[credit['details']], 'debit':[], 'nett':credit['details']['amount']}
                    self.confirmed_transactions.insert_one(new_credit)
        
    def find_block(self, blocknum):
        """ Find the accounts that have transactions corresponding
        to blocknum
        """

        # Get list of on hold transactions
        trans_list = self.hold_transactions.find()

        ret_debits = []
        ret_credits = []

        for trans in trans_list:
            debits = [{"ID":trans['ID'], "details":
            {'blocknum':blocknum, 'signature':x['signature'],
            'amount':x['amount']}} for x in trans['debit']
            if x['blocknum'] == blocknum]

            ret_debits.extend(debits)

            credits = [{"ID":trans['ID'], "details":
            {"blocknum":blocknum, "signature": x["signature"], 
            "amount":x["amount"]}} for x in trans['credit']
            if x['blocknum'] == blocknum]

            ret_credits.extend(credits)

        return ret_debits, ret_credits

    def finalize(self, blocknum):
        """ Actually apply the transactions in blocknum to the balance
        """

        # Locate all debits and credits from the block that was just confirmed.
        debits, credits = self.find_block(blocknum)

        # Let's process all the debits first

        if len(debits) < 1 and len(credits) < 1:
            print("No transactions to finalize for block %d." % blocknum)
            return 

        for debit in debits:
            from_id = debit["ID"]
            from_query = {"ID":from_id}

            # Just as a caution we will ensure we have no negative balance
            bal = self.get_balance(from_id)
            from_bal = bal['balance']

            if from_bal < 0:
                from_bal = 0

            debit_amount = debit['details']['amount']
            nett_bal = from_bal - debit_amount

            if nett_bal >= 0.0:
                if self.balances.count_documents(from_query) > 0:
                    self.balances.update_one(from_query, {"$set":{'balance': nett_bal}})
                else:
                    self.balance.insert_one({"ID":from_id, "balance": nett_bal})
            else:
                print("Transaction leads to negative balance. Discarding.")
            
        for credit in credits:

            to_id = credit["ID"]
            to_query = {"ID":to_id}
            bal = self.get_balance(to_id)
            to_bal = bal['balance']

            if to_bal < 0:
                to_bal = 0

            credit_amount = credit['details']['amount']
            nett_bal = to_bal + credit_amount

            if self.balances.count_documents(to_query) > 0:
                self.balances.update_one(to_query, {"$set":{'balance': nett_bal}})
            else:
                self.balances.insert_one({"ID":to_id, "balance":nett_bal})

            #print("FINALIZE: SUCCESS")

        # Adjust the netts
        self.adjust_netts(debits, credits)

    def open_account(self, ID, public_key):
        """ Create a new account
        """

        # Check if ID and public key match

        if not chain_utils.verify_wallet_ID(public_key, ID):
            print("ACCOUNT: ID and public key do not match.")
            return False

        # Check if ID already exists
        
        if self.check_account_exists(ID):
            print("ACCOUNT: Account already exists.")
            return False

        # Create the account
        account = {"ID":ID, "public_key":public_key}
        result = self.accounts.insert_one(account)

        # Create a zero balance
        balance = {"ID":ID, "balance":0.0}
        result = self.balances.insert_one(balance)

    def validate_wipe(self):
        # Uncomment below to short out the validation
        #return True

        val = input("Enter COI user's ID to validate: ")
        return val == self.cic_user['ID']

    def wipe(self):
        """ Wipe out database """
        validate = self.validate_wipe()
        if validate:
            self.accounts.drop()
            self.balances.drop()
            self.hold_balances.drop()
            self.transactions.drop()
            self.hold_transactions.drop()
            self.confirmed_transactions.drop()
        else:
            print("CANCEL WIPE")
