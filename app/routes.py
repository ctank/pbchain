from flask import Flask
import pbmain
from app import app
import requests
from flask import request
import json

app = Flask(__name__)

def outcome(ret):
    if ret is not None:
        return json.dumps(ret), 200
    else:
        return "Error.", 500

@app.route('/', methods = ['GET'])
def root_fn():
    ret =  {"title":"PBChain", "version":"0.1"}
    return outcome(ret)


@app.route('/notify_new_block', methods = ['POST'])
def notify_new_block():
    block = request.json()
    ret =  pbmain.pb_network.handle_block_broadcast(block)
    return outcome(ret)


@app.route('/notify_new_transaction', methods = ['POST'])
def notify_new_transaction():
    transaction = request.json()
    ret =  pbmain.pb_network.handle_new_transaction(transaction)
    return outcome(ret)

@app.route('/get_chain_len', methods = ['GET'])
def get_chainlen():
    retlen = pbmain.pb_network.handle_get_blockchain_len()

    ret = {'chain_len': retlen}
    return outcome(ret)

@app.route('/get_chain', methods = ['GET'])
def get_chain():

    indexstr = request.args.get('index')

    if indexstr is not None:   
        index = int(indexstr)
    else:
        index = 0

    if index is not None:
        blockchain =  pbmain.pb_network.handle_get_blockchain(index)

    if blockchain is not None:
        ret = []
        for block in blockchain:
            ret.append(block.__dict__)
    else:
        return "Invalid chain.", 500
    return outcome(ret)

@app.route('/register_peer', methods = ['POST'])
def register_peer():
    req = request.json()
    ret = pbmain.pb_network.handle_peer_add_request(req)
    return outcome(ret)

@app.route('/get_peers', methods = ['GET'])
def get_peers():
    retlist =  pbmain.pb_network.handle_get_peers()
    ret = {"peers":retlist}
    return outcome(ret)

@app.route('/get_confirmed_transactions', methods = ['GET'])
def get_confirmed_transactions():
    ret =  pbmain.pb_network.handle_get_confirmed_transactions(int(request.args.get('index')))
    return outcome(ret)

@app.route('/get_unconfirmed_transactions', methods = ['GET'])
def get_unconfirmed_transactions():
    ret =  pbmain.pb_network.handle_get_unconfirmed_transactions()
    return outcome(ret)

if __name__ == '__main__':
    flask.run(port = 5000)
