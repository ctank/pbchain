import blockchain
import storage
import httpnet
import blockchain
import chain_utils
import threading
import accounts
import json
import time

class PBChain:
    def __init__(self, pb_conf, user_conf):
        """ Constructor. pb_conf = File containing config information for
        this blockchain. user_conf = Wallet file for a user containing
        ID, public key and private key.
        """

        with open(pb_conf) as f:
            self.config = json.load(f)
        
        self.user = chain_utils.load_user(user_conf)

        dbname = self.config['dbname']
        init_difficulty = self.config['difficulty']
        db_connect = self.config['db_connect_string']
        nw_connect = self.config['nw_connect_string']
        protocol = self.config['protocol']
        coin_db = self.config['coin_db']
        N = self.config['confirm_depth']
        M = self.config['fork_confirm_depth']

        # Mining interval in seconds
        self.mining_interval = self.config['mining_interval']

        # Network information
        self.host = self.config['host']
        self.port = self.config['port']

        self.db = storage.Storage(dbname = dbname, connectString = db_connect, M = M)
        self.bc = blockchain.Blockchain(init_difficulty = init_difficulty, N = N)
        self.nw = httpnet.HTTPNet(blockchain = self.bc, protocol = protocol)
        self.coin = accounts.Accounts(database = coin_db)
        self.coin.init_cic()
        
        self.seedpeer = None

        # Set up the blockchain
        self.bc.set_storage(self.db)

        if 'seedpeer' in self.config:
            if 'host' in self.config['seedpeer'] and 'port' in self.config['seedpeer']:
                self.seedpeer = self.config['seedpeer']
                self.bc.set_seedpeer(self.seedpeer['host'], self.seedpeer['port'])

        self.bc.set_network(self.nw)
        self.bc.set_coin(self.coin)
        self.bc.set_user(self.user)
        self.bc.set_self_info(self.host, self.port)

        # Run flag
        self.run = False

    def get_network(self):
        """ Return network object, host name and port
        """

        return self.nw, self.host, self.port

    def start(self):
        """ Starts the mining thread and registers self with peers
        """

        def _mining_thread_():
            while self.run:
                # Wait for # of seconds specified in mining_interval
                time.sleep(self.mining_interval)

                print("Mining..")
                self.bc.mine_block()

        # Go through every
        # Start the mining thread. Mines every self.mining_interval seconds

        self.run = True

        print("Registering with peers.")
        self.bc.register_with_peers()

        th = threading.Thread(target = _mining_thread_)
        th.start()

    def stop(self):
        """ Stops the mining thread
        """

        self.run = False

