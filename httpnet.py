import network
import requests
import json

class HTTPNet(network.NetworkBase):
    def __init__(self, blockchain, connect_string = '', protocol = 'http'):
        """ Initialize the HTTP object
        """

        super().__init__(blockchain = blockchain)
        self.json_headers = {"Content-type": "application/json"}

        if '://' not in protocol:
            protocol = protocol + '://'

        self.protocol = protocol

    def check_fields(self, the_dict, field_list):
        """ Returns True if all fields in fields_list are in the_dict
            else False
        """

        for field in field_list:
            if not field in the_dict:
                return False

        return True

    def make_req(self, peer, endpoint):
        """ Form a HTTP request. 

            Params:
                peer = Dict of the form {"host":<hostname>, "port":<portnum>}
                endpoint = String of the form "/<endpoint>".
            Returns:
                A string of the form "http://<host>:<port>/<endpoint
        """

        if not endpoint.startswith('/'):
            endpoint = '/' + endpoint

        return self.protocol + peer['host'] + ':' + str(peer['port']) + endpoint

    def broadcast(self, peers, endpoint, data):
        """ Broadcasts data to all peers in peers list.

            Params:
                peers: List of peers of the form [{'host'<host1>, 'port':<port1'}, {'host':<host2> ... etc]
                endpoint: Endpoint to send to
        """

        print("BROADCAST: PEER LIST: ", peers)
        result = []
        for peer in peers:
            if self.check_fields(peer, ['host', 'port']):
                req = self.make_req(peer, endpoint)

                try:
                    r = requests.post(req, data = json.dumps(data), headers = self.json_headers, timeout = 1.0)

                    if r.status_code == 200:
                        print("Sent to %s:%s." % (peer['host'], str(peer['port'])))
                        print("YAAY")
                    else:
                        print("Status code: %d" % r.status_code)

                    result.append({"peer":peer, "result":r.status_code})

                except Exception as e:
                    print("Broadcast exception: ", e)
            else:
                print("Illegal peer: ", peer)

        return result

    def broadcast_block(self, peers, block):
        """ Broadcast a newly mined block to all peers
        """

        self.broadcast(peers, '/notify_new_block', block)

    def broadcast_transaction(self, peers, sender_id, receiver_id, transaction):
        """ Broadcast a newly received transaction to all peers
        """

        trans_obj = {"sender_id" : sender_id, "receiver_id": receiver_id,
        "transaction":transaction}

        self.broadcast(peers, '/notify_new_transaction', trans_obj)

    def request_chain_len(self, peer):
        """ Request chain length from a peer
        """

        if self.check_fields(peer, ['host', 'port']):
            req = self.make_req(peer, '/get_chain_len')

            r = requests.get(req, timeout = 1.0)

            if r.status_code == 200:
                ret = r.json()
            else:
                print("Get chain len: ", r)
                ret = None
        else:
            print("Illegal peer: ", peer)
            ret = None

        return ret

    def broadcast_request_chain_len(self, peers):
        """ Request chain lengths from all peers. Returns as a list
        of {"peer":<peer>, "len":<length>}
        """

        retlist = []
        for peer in peers:
            try:
                chain_len = self.request_chain_len(peer)
                if chain_len is not None:
                    ret = {"peer":peer, "len":chain_len}
                    #print("Peer length data: ", ret)
                    retlist.append(ret)
            except:
                print("Unable to connect to peer ", peer)

        #print("Retlist: ", retlist)
        return retlist

    def download(self, peer, end_point, start_index):
        """ Download a blockchain or transaction from a given starting index
        """

        if self.check_fields(peer, ['host', 'port']):
            chain_req = {"index": start_index}
            req = self.make_req(peer, end_point)

            print("DOWNLOAD ENDPOINT: ", req, " Req: ", chain_req)
            r = requests.get(req, params = chain_req)

            if r.status_code == 200:
                ret = r.json()
            else:
                print("Error: %d." % r.status_code)
                ret = None
        else:
            print("Illegal peer: ", peer)
            ret = None

        return ret

    def download_chain(self, peer, start_index):
        """ Download the blockchain from a given peer
        """

        print("DOWNLOAD CHAIN: ", peer)
        return self.download(peer,  '/get_chain', start_index)

    def download_confirmed_transactions(self, peer, start_index):
        """ Get confirmed transactions from a given index
        """

        return self.download(peer, '/get_confirmed_transactions', start_index)

    def download_unconfirmed_transactions(self, peer):
        """
            Get unconfirmed transactions. We always start from 0
        """

        return self.download(peer, '/get_unconfirmed_transactions', 0)

    def register_self_to_peer(self, peer, host, port, netid):
        """
            Register self to other peers
        """

        data = {"host":host, "port":port, "netid":netid}
        req = self.make_req(peer, '/register_peer')
        print("\n\nREGISTER: %s" % req)
        print("REGISTER WITH %s:%d:%s." % (host, port, netid))

        try:
            r = requests.post(req, headers = self.json_headers, data = json.dumps(data))
            return r.status_code, r.json()
        except:
            print("Cannot connect to peer.")
            return 500, None

