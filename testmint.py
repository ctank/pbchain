import accounts
import chain_utils
import sys

if len(sys.argv) != 2:
    print("\nUsage: %s <database name>\n\n" % sys.argv[0])
    exit(-1)

u1 = chain_utils.load_user('polarbear')
u2 = chain_utils.load_user('kucinta')

a = accounts.Accounts(database = sys.argv[1])
a.init_cic()

a.mint(u1['ID'], 25)
a.mint(u2['ID'], 18)

a.settle(0)
a.finalize(0)
