import requests
import chain_utils
import json
import time

print("\n\nTEST")
print(    "----\n\n")

user1 = chain_utils.load_user('polarbear')
user2 = chain_utils.load_user('kucinta')

t1 = {'cmd':'pay', 'type':1, 'from':user1['ID'], 'to':user2['ID'], 'amt':300}
t2 = {'cmd':'pay', 'type':2, 'from':user2['ID'], 'to':user1['ID'], 'amt':150}
t3 = {'cmd':'test', 'type':3, 'from':user1['ID'], 'to':user2['ID']}
t4 = {'cmd':'test again', 'type':0, 'from':user1['ID'], 'to':user2['ID'], 'cmd':1, 'amount':1.1}

trans1 = chain_utils.make_transaction(t1, user1['ID'], user2['ID'], user1['key'])
trans1['signature']=''
trans2 = chain_utils.make_transaction(t2, user2['ID'], user1['ID'], user2['key'])
trans3 = chain_utils.make_transaction(t3, user1['ID'], user2['ID'], user1['key'])
trans4 = chain_utils.make_transaction(t4, user1['ID'], user2['ID'], user1['key'])

headers = {'Content-type':'application/json'}

def make_trans(user1, user2, trans):
#    return json.dumps({'senderID':user1, 'receiverID':user2, 'transaction':trans})
    return json.dumps(trans)

# Add transactions
print("Adding transaction")

send_trans = make_trans(user1['ID'], user2['ID'], trans1)
print(send_trans)

# Add a peer
peer = {"host":"127.0.0.1", "port":5002}

#res = requests.post('http://127.0.0.1:5002/register_peer', headers = headers, 
#data = json.dumps(peer))
#print("Add peer result: %d." % res.status_code)
#print("Response: ", res.json())

res = requests.post('http://127.0.0.1:5001/add_new_transaction', headers=headers, 
data = send_trans)
print("Add transaction t1 result: %d." % res.status_code)

time.sleep(1)

send_trans = make_trans(user2['ID'], user1['ID'], trans2)
res = requests.post('http://127.0.0.1:5001/add_new_transaction', headers=headers, 
data = send_trans)
print("Add transaction t2 result: %d." % res.status_code)

time.sleep(1)

send_trans = make_trans(user1['ID'], user2['ID'], trans3)
res = requests.post('http://127.0.0.1:5001/add_new_transaction', headers=headers, 
data = send_trans)
print("Add transaction t3 result: %d." % res.status_code)

time.sleep(1)

send_trans = make_trans(user1['ID'], user2['ID'], trans4)
res = requests.post('http://127.0.0.1:5001/add_new_transaction', headers=headers, 
data = send_trans)
print("Add transaction t4 result: %d." % res.status_code)

#time.sleep(2)
#print("Trying to resend again. See how.")

#res = requests.post('http://127.0.0.1:5002/add_new_transaction', headers=headers, 
#data = send_trans)
#print("Add transaction result: %d." % res.status_code)
