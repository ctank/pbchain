from block import Block
import chain_utils
import merkletools
import storage
import json
import pbcoin
import threading
import socket
from Crypto.Hash.SHA256 import SHA256Hash
import constants

#
# Dummy function that always returns True
#

def returnTrue(trans_or_block):
    #print("BOO!")
    return True

# For mined blocks
def mined_hook(block, trans_list):
    #print("BAH")
    return True

class Blockchain:
    """ This is the Blockchain class that provides services for:
        - Maintaining a set of unconfirmed transactions
        - Maintaining a set of confirmed transactions
        - Maintaining a chain of blocks to verify the transactions
        - Verifying transaction signatures before adding them
        - Providing verification callbacks when a new transaction
            is proposed
        - Providing action callbacks when a new transaction is 
            mined and approved for adding
        - Providing action callbacks when a transaction has a further
            N blocks added after that. Default N = 6
    """

###
### Class initializer
###

    def __init__(self, init_difficulty = 3, N = 6):
        """ Initialize the blockchain 
            params:
                storage: Storage class to store the chain, unconfirmed and confirmed transactions.

                N: Number of blocks to be added first before
                    we are notified again that the block has been
                    approved. The future additions reassure us that
                    this transaction is approved by the miners

                init_difficulty: Initial difficulty level. This is increased if there are
                on average more than one new block every 10 minutes, and decreased if there
                are transactions, yet there are fewer than one new block every 10 minutes.

                Note that if there are no transactions, this number is not adjusted.
        """

        self.storage = None
        self.network = None
        self.user_storage = None

        self.difficulty = init_difficulty
        self.N = N
        self.num_unconfirmed = 0
        self.num_confirmed = 0
        self.chain_length = 0
        self.num_trans = 0
        self.user = None
        self.coin = None
        self.lock = threading.Lock()
        self.seed_peer = None
        self.host = ''
        self.port = 999
        self.longest_chain_len = 0
        self.longest_chain_peer = None
        self.seedpeer = None

        # Callbacks

        # Called when a new transaction is proposed, after
        # digital signatures and ownership are verified.
        # Takes a list of transaction dictionaries that is unique to each application.
        # Return TRUE if new transaction is OK, FALSE otherwise
        self.verify_transaction_hook = returnTrue

        # Called when a new transaciton is mined.
        # Takes the mined block and list of transactions.
        # Return TRUE if successfully processed, FALSE otherwise
        self.new_transaction_mined_hook = mined_hook

        # Called when a new transaction is below N new blocks
        # Return TRUE if successfully processed, FALSE otherwise
        self.transaction_confirmed_hook = returnTrue

###
### Fucntions to set important class parameters: User, network, storage and coin
###

    def set_user(self, user):
        """ Set the user for this blockchain for mining.
        If no user is set you will not be able to mine
        """

        self.user = user

    def set_network(self, network):
        """ Set the network communications object.
        """

        self.network = network

    def set_storage(self, storage):
        """ Set the storage object.
        """

        self.storage = storage

        if self.storage.get_blockchain_len() == 0:
            print("Blockchain is empty. Making genesis block.")
            self.make_genesis_block()

    def set_coin(self, coin):
        """ Set the coin object for mining and other payments
        """

        self.coin = coin

    def set_user_storage(self, user_storage):
        """ Sets the user storage for user scripts
        """

        self.user_storage = user_storage
###
### Set seed peer and own info
###

    def set_seedpeer(self, host, port):
        hostip = socket.gethostbyname(host)
        seedpeer = {'host':hostip, 'port':port, 'netid':None}
        self.seedpeer = seedpeer
        self.storage.set_seedpeer(seedpeer)

    def set_self_info(self, host, port):
        self.host = socket.gethostbyname(host)
        self.port = port

        self.storage.set_self_info(host, port)

###
### Functions to handle new transactions
###

    def check_fields(self, trans, field_list):
        """ Check that all fields specified are in
        the dictionary trans """

        for x in field_list:
            if x not in trans:
                #print("Field %s is missing from transaction." % x)
                return False

        return True

    def is_pbcoin_transaction(self, trans):
        """ Check if this is a coin transaction. If so
        the 'type' field must be present and set to 0
        """

        return 'type' in trans and trans['type'] == constants.COIN_OBJECT

    def verify_pbcoin_transaction(self, transaction, signature):
        """ Verify PBCoin transaction
        """

        if 'from' not in transaction or 'to' not in transaction or 'amount' not in transaction:
            print("ILLEGAL COIN PACKET")
            return False

        # This is not a coin transaction
        if not self.is_pbcoin_transaction(transaction):
            print("This is not a coin transaction")
            return True

        if self.coin is None:
            print("No coin initialized in blockchain")
            return False

        if transaction['cmd'] == constants.PAY_COIN:
            print("Payment of %3.2f from %s to %s." 
            % (transaction['amount'], transaction['from'],
            transaction['to']))
            res = self.coin.transfer(transaction['from'], transaction['to'],
            transaction['amount'], signature)
        else:
            print("UNRECOGNIZED INSTRUCTION")
            res = False

        return res


    def verify_transaction(self, sender_id, receiver_id,
    transaction, check_unconfirmed = True, check_signature = True):
        """ Check that the transaction is valid 
        By default it will also check for duplicates in the unconfirmed
        transactions list.
        """

        if transaction is None:
            return False

        if not self.check_fields(transaction, ['transaction']):
            print("Malformed transaction object.")
            return False

        trans = transaction['transaction']

        if not self.check_fields(trans, ['from', 'to']):
            print("Malformed application transaction record.")
            return False

        if trans["from"] != sender_id:
            print("Sender ID mismatch.")
            return False

        if trans["to"] != receiver_id:
            print("Receiver ID mismatch.")
            return False

        if not chain_utils.verify_transaction(transaction, check_signature):
            print("Transaction integrity test failed.")
            return False

        if check_unconfirmed:
            if self.storage.check_in_unconfirmed(transaction):
                print("Repeat transaction. Deleted.")
                return False
        
        if self.is_pbcoin_transaction(trans):
            print("This is a new coin transaction")
            if not self.verify_pbcoin_transaction(trans, transaction['signature']):
                print("PBCoin Transaction failed.")
                return False
        else:
            if not self.verify_transaction_hook(transaction["transaction"]):
                print("Transaction feasibility test failed.")
                return False

        return True

    def send_new_transaction(self, sender_id, receiver_id, 
    transaction, broadcast = True, check_signature = True):
        """ PUBLIC: Send in a new transaction to the chain.

        Params:
            sender_id, receiver_id: Wallet IDs for sender and receiver.
            transaction: Transaction object as created by chain_utils.make_transaction
        # Check that the transaction is valid.
        """


        if check_signature is False:
            # See if this is an agent
            if self.userStorage is None:
                print("Signature checking disable. Unable to see if this is an agent.")
                return False
            else:
                agent = self.userStorage.get_agent(sender_id)
                if not agent:
                    print("Not an agent. Rejecting.")
                    return False

        if self.verify_transaction(sender_id, receiver_id, transaction, check_signature):
            self.num_unconfirmed = self.storage.add_trans_to_unconfirmed(transaction)
            
            if broadcast:
                # Broadcast the transaction
                peers = self.storage.get_peers()
                #print("BROADCASTING TRANSACTIONS TO " , peers)
                self.network.broadcast_transaction(peers,
                sender_id, receiver_id, transaction)

            return True

        else:
            print("Transaction check failed.")
            return False

###
### Creation of Genesis Block
###

    def make_genesis_block(self):
        """ Create the genesis block and add it to the blockchain

            Params: None
            Returns: None
        """

        block = Block(index = 0, data_hash = 0, prev_hash = 0, difficulty = self.difficulty, genesis = True)
        self.chain_length = self.storage.add_genesis_block(block)
        return block
###
### Mining related functions
###

    def broadcast_block(self, trans_list, block):
        """
            Broadcast a block to all peers
        """

        peer_list = self.storage.get_peers()
        output = {"trans_list":trans_list, "block":block.__dict__}
        ret = self.network.broadcast_block(peer_list, block = output)

    def new_pbcoin_block(self, block, trans):
        """ Handle PBCoin transactions
        This is called when a block is mined. Settles proposed transactions
        
        """

        # We store our transactions so we actually don't need trans!
        if self.coin is None:
            print("Coin not initialized.")
            return False

        self.coin.settle(blocknum = block.index)

    def confirm_pbcoin_transaction(self, block):
        """ Block has been confirmed. We confirm PBcoin transactions.
            Called when a block has been buried under N-1 other
            blocks.
        """

        print("Confirm PB coin block")

        if self.coin is None:
            print("Coin not initialized.")
            return False

        self.coin.finalize(blocknum = block.index)

    def handle_mint(self, mint_trans, blocknum):
        """
        Mints a new coin when a mining is complete.
        """

        print("Paying %s for mining." % mint_trans['to'])
        self.coin.transfer(from_id = 'cic', to_id = mint_trans['to'],
        amount = 0.5, signature = 'nosig')
        self.coin.settle(blocknum)

        pass

    def confirm_blocks(self):
        """
        Once a block has been buried under N-1 blocks, we take that it is
        confirmed. For coins this will move balances from the hold balance
        to the actual balance.
        """

        end_block = self.storage.get_blockchain_len() - self.N

        if end_block > 0:

            for block_num in range(self.storage.get_last_confirmed() + 1,
            end_block + 1):
                
                print("CONFIRM: Confirming block %d." % block_num)

                cfm_block = self.storage.get_nth_block(block_num)

                if cfm_block is not None:
                    self.confirm_pbcoin_transaction(cfm_block)
                    self.transaction_confirmed_hook(cfm_block)
                    self.storage.set_last_confirmed(cfm_block.index)

###
### Agent Actions
###

    def agent_mined_action(self, trans_list):
        pass

###
### Mining action functions
###

    def common_mined_block_action(self, block, trans_list):
        """ Actions common to locally mined block
        and blocks from other peers
        """

        if len(trans_list) < 2:
            print("Invalid transaction list.")
            return True

        first = trans_list[0]['transaction']

        if self.is_pbcoin_transaction(first):
            if first['cmd'] == constants.MINT_COIN:
                #print("MINT TRANSACTION: ", first)
                self.handle_mint(first, block.index)

        # Extract out pbcoin transactions
        # These have a "type" field and set to 0. We also filter
        # out mint transactions.
        pb_trans = [trans['transaction'] for trans in trans_list
        if self.is_pbcoin_transaction(trans['transaction'])
        and trans['transaction']['cmd'] != constants.MINT_COIN]

        agent_trans = [trans['transaction'] for trans in trans_list
        if trans['transaction']['type'] == constants.AGENT_OBJECT]


        self.agent_mined_action(agent_trans)

        # Extract the rest
        other_trans = [trans['transaction'] for trans in trans_list
        if (trans['transaction'] not in pb_trans and trans['transaction'] not in agent_trans)
        and not self.is_pbcoin_transaction(trans['transaction'])]

        print("\n\n**** AGENT TRANS: ", agent_trans, " *****")
        print("****PBCOIN TRANS: ", pb_trans, " ****")
        print("****Other trans: ",  other_trans, " ****\n\n")

        # Handle PBCount transactions
        if len(pb_trans) > 0:
            self.new_pbcoin_block(block, pb_trans)
        else:
            print("No coin transactions mined.")

        # Invoke callback

        if len(other_trans) > 0:
            self.new_transaction_mined_hook(block, other_trans)
        else:
            print("No other transactions mined.")

        # Confirm the blocks that are N blocks away
        self.confirm_blocks()

        return True
        

    def local_mined_block_action(self, block, trans_list):
        """ These are actions for blocks mined locally
        """

        # Move to confirmed transaction
        self.storage.move_trans_to_confirmed(block.index)

        print("\n\n***** TRANS LIST: ", trans_list, "*****\n\n")
        hook_result = self.common_mined_block_action(block, trans_list)

        # Broadcast the block
        self.broadcast_block(trans_list, block)

        return hook_result

    def check_no_double_count(self, trans_list):
        """ Checks to ensure that transactions in a block
        are not executed twice
        """

        return trans_list

        # Ensure that there is no double-counting of the same transaction
        conf_list = self.storage.get_confirmed_transactions(0)

        if trans_list in conf_list:
            print("Failed at point 1")
            return False

        # Run through each batch of transactions in conf_list
        # See if this transaction is there.

        new_trans = []
        for trans in trans_list:
            for conf in conf_list:
                if trans in conf['transactions']:
                    print("Failed at point 2")
                    print("trans: ", trans)
                    print("conf: ", conf['transactions'])
                    print("REMOVED")
                else:
                    new_trans.append(trans)

        print("NO DOUBLE COUNTS!")
        return new_trans

    def mine_block(self):
        """ PUBLIC: Mine the current unconfirmed transactions.

            Params: None
            Returns: True if a new block was mined. False otherwise
        """

        # Lock
        self.lock.acquire()
        new_block = None

        try:
            if self.user is None:
                print("Set your user before mining.")
                return None

            # Get the unconfirmed transactions.
            trans_list = self.storage.get_unconfirmed_transactions()

            if self.storage.get_unconfirmed_transactions_count() > 0:

                # Insert a mint transaction

                mint = pbcoin.mint_pbcoin(self.user['ID'], self.user['key'])

                print("Minted new coin for user %s." % self.user['ID'])
                self.storage.add_mint_to_unconfirmed(mint)

                trans_list = self.get_unconfirmed_transactions()
                trans_list = self.check_no_double_count(trans_list)

                # Have at least two transactions, otherwise we exit
                # The first is the mint transaction we just inserted.
                # The second is whatever other transactions there are.
                if len(trans_list) < 2:
                    print("List is empty. Wiping unconfirmeid.")
                    self.storage.wipe_unconfirmed()
                    return None

                # Compute merkle tree
                mtree = merkletools.MerkleTools()

                # Add each transaction to the tree
                for trans in trans_list:
                    mtree.add_leaf(json.dumps(trans, sort_keys = True), do_hash = True)

                mtree.make_tree()

                # Get the Merkle root hash
                data_hash = mtree.get_merkle_root()

                # Get the previous block
                prev_block = self.storage.get_last_block()

                prev_hash = prev_block.hash
                prev_index = prev_block.index

                # Mine the new block. The Block constructor immediately mines for the hash
                new_block = Block(index = prev_index + 1, data_hash = data_hash, prev_hash = prev_hash, difficulty = self.difficulty)

                # Now check that the current block is still the previous block; otherwise we have a possible fork.
                # Note: Since Block takes a while due to the crypto puzzle, it's possible that another block has been added
                if self.storage.add_block_to_chain(new_block) > 0:
                    self.local_mined_block_action(new_block, trans_list)
                    return new_block
                else:
                    return None
            else:
                print("No transactions to mine.")
                return None
        except Exception as e:
            print("Mining exception: ", e)
            raise
        finally:
            self.lock.release()
            return new_block

###
### Functiosn to verify blocks from other peers
###

    def verify_proof_of_work(self, block):
        """ This function checks that the proof of work is valid

            Params: 
                block: Block to check

            Returns:
                True if POF is valid, False otherwise
        """

        # Ensure that the block difficulty is not significantly lower than the current level
        if (block.difficulty < self.difficulty) and (self.difficulty - block.difficulty > 1):
            print("Block difficulty too low.")
            return False

        # Save the block hash
        hash = block.hash

        # Check that is has the correct format
        if not hash.startswith('0' * block.difficulty):
            print("Wrong numner of '0'")
            return False

        # Delete the attribute and find the hash
        #delattr(block, 'hash')
        block.hash = 0

        block_hash = SHA256Hash(json.dumps(block.__dict__, sort_keys = True).encode()).hexdigest()

        if block_hash != hash:
            print("Block hash does not match.")
            return False

        # Put back the hash
        block.hash = hash

        # Successfully checked out the proof of work.
        return True

    def make_block_from_json(self, block_json):
        """ Converts a block from JSON form transmitted by peers
        to proper block objects
        """

        # Suppress generation of hash
        new_block = Block(index = block_json["index"], 
        data_hash = block_json["data_hash"],
        prev_hash = block_json["prev_hash"],
        difficulty = block_json["difficulty"], do_hash = False)

        new_block.hash = block_json["hash"]
        new_block.nonce = block_json["nonce"]
        new_block.time_stamp = block_json["time_stamp"]

        return new_block

    def verify_merkle_tree(self, block, trans_list):
        """ Verify that the Merkle Tree root for trans_list
        matches the hash in the block.

            Params: 
                block: Block to verify
                trans_list: List of transactions to verify

            Returns:
                True if Merkle root matches with data hash in block, false otherwise

        """
    
        # When we verify a chain from another host, the first
        # block is actually the previous block, and has no
        # transactions associated with it. Hence we set trans_list to []
        # and we do not verify the Merkle hash.

        if trans_list == []:
            return True

        mtree = merkletools.MerkleTools()

        for trans in trans_list:
            mtree.add_leaf(json.dumps(trans, sort_keys = True), do_hash = True)

        mtree.make_tree()

        data_hash = mtree.get_merkle_root()

        return data_hash == block.data_hash


    def verify_block(self, inblock, prev_block):
        """ Verify a block received from a peer.
        """

        #print("VERIFY BLOCK:", inblock)
        block = self.make_block_from_json(inblock['block'])
        trans_list = inblock['trans_list']

        # If this is the genesis block, just return it.

        if block.index == 0:
            return block

        # Check previous hash
        if block.prev_hash != prev_block.hash and block.prev_hash != prev_block.prev_hash:
            print("Previous block fail.")
            print("\t** Prev block hash: %s This block prev hash: %s." %
            (prev_block.hash, block.prev_hash))

            return None

        # Check that the Merkle Tree roots match
        if not self.verify_merkle_tree(block, trans_list):
            print("Failed Merkle Test.")
            return None

        # Check proof of work
        if not self.verify_proof_of_work(block):
            print("Failed POW test.")
            return None

        return block

    def add_block_from_other_peers(self, in_block):
        """ Add in a block received from a peer.
            
            This function is called by the networking component when it receives a block broadcast

            Params:
                in_block: The incoming block annoucnement. It is a JSON of this form:
                    in_block['block']: The actual block
                    in_block['trans_list']: The transaction list in this block

            Returns:
                True if block is added, False otherwise. Also triggers transaction_mined_hook if block is added
        """

        trans_list = in_block['trans_list']

        trans_list = self.check_no_double_count(trans_list)

        prev_block = self.storage.get_last_block()

        block = self.verify_block(in_block, prev_block)

        if block is not None:
            # All ok, add to the blockchain.
            if self.storage.add_block_to_chain(block) > 0:
                # Remove transactions in the block from the unconfirmed list
                # so we don't double count
                self.storage.remove_from_unconfirmed(trans_list)

                # Move to confirmed list
                self.storage.add_to_confirmed(block.index, trans_list)

                # Call the common actions that include minting a new
                # coin and processing the transaction
                self.common_mined_block_action(block, trans_list)
                return block
            else:
                return None

###
### Miscellaneous block management
###
    def flush_block_chain(self):
        self.storage.flush_block_chain()

    def get_blockchain(self, index = 0):
        return self.storage.get_blockchain(index)

    def get_blockchain_len(self):
        return self.storage.get_blockchain_len()

    def get_confirmed_transactions(self, index):
        return self.storage.get_confirmed_transactions(index)

    def get_unconfirmed_transactions(self):
        return self.storage.get_unconfirmed_transactions()

    def consensus(self, peer, res):
        print("Peer: ", peer, " Result: ", res)

    def get_netid(self):
        return self.storage.get_netid()

###
### Peer Management Functions
###

    def add_peer(self, peer, no_netcheck = False):
        """ Add a new peer. Set no_netcheck to True to
        suppress checking of network ID """

        return self.storage.add_peer(peer, no_netcheck)

    def get_peers(self):
        """ Return the set of peers
        """

        return self.storage.get_peers()

    def add_peers_to_set(self, peerset, peerlist):
        """ Adds a list of peers into an existing set
        """

        tmp_peers = [peer for peer in peerlist if peer not in peerset]
        peerset.extend(tmp_peers)

    def download_longest_chain(self, seed_download = False):
        """ Download the longest chain from amongst the peers """

        self.longest_chain_len = self.storage.get_blockchain_len()
        if seed_download and self.longest_chain_len < 2:
            self.longest_chain_len = 0

        peers = self.storage.get_peers()

        peer_lengths = self.network.broadcast_request_chain_len(peers)

        for peer_length in peer_lengths:
            print("PEER LENGTH: ", peer_length)
            if peer_length['len']['chain_len'] > self.longest_chain_len:
                self.longest_chain_len = peer_length['len']['chain_len']
                self.longest_chain_peer = peer_length['peer']

        if self.longest_chain_peer is not None:
            self.copychain(self.longest_chain_peer)
        else:
            print("No peer to download chain from.")

    def register_with_peers(self):
        """ Register self with peers. This will get the peer list
        from peers, and then we connect with those as well.
        """

        peer_set = self.storage.get_peer_set()

        # Assume we are not downloading from the seed peer.
        seed_download = False

        print("Peer set: ", peer_set)
        self.longest_chain_len = self.storage.get_blockchain_len()

        #print("My chain length: " , self.longest_chain_len)

        newpeers = []

        if self.storage.get_blockchain_len() < 2:
            self.netid = None
        else:
            self.netid = self.storage.get_netid()

        #print("My network ID: ", self.netid)

        # If peer list is empty:
        if len(peer_set) == 0:
            # Register with the seed peer if any

            seed_download = True
            print("Peer set is empty. Trying to get from seed peer.")

            if self.seedpeer is not None:
                try:
                    # If our blockchain only has a gensis block, we use
                    # a netid of None. It means our chain is brand new

                    print("Seed peer is defined. Contacting..")
                    # Get its peer list and add to new_peers

                    status, res = self.network.register_self_to_peer(self.seedpeer, 
                    self.host, self.port, self.netid)

                    if status == 200:
                        # Check that the network ID matches ours

                        if 'netid' in res:
                            if res['netid'] == self.netid or self.netid is None:
                                self.seedpeer['netid'] = res['netid']

                                # Add seedpeer to newpeers so we download the
                                # chain from there.

                                newpeers = [self.seedpeer]
                                self.add_peers_to_set(newpeers, res['peers'])
        
                                print("Seed peer has a chain length of %d" % res['chainlen'])
                                print("ADDING SEED PEER TO BLOCKCHAIN")
                                # Add seed peer to our own peer list
                                self.add_peer(self.seedpeer)
                                #print("\n*** PEERS: ", self.storage.peers,"***\n\n")
                        else:
                            print("Error: ", res)
                except Exception as e:
                    print("Error registering peer: ", e)
                    raise
            else:
                print("NO SEED PEER DEFINED.")
        else:
            newpeers = peer_set

        myhostdata = {'host':self.host, 'port':self.port, 'netid':self.get_netid()}

        while len(newpeers) > 0:
            # Remove self
             
            # Remove self from newset

            newpeers = [x for x in newpeers if x['host'] != myhostdata['host'] or x['port'] != myhostdata['port']]

            for peer in newpeers:
                # Delete the peer
                print("BEFORE: ", newpeers, " PEER: ", peer)
                newpeers = [x for x in newpeers if x != peer]
                print("AFTER: ", newpeers)

                # Now get its peer list

                print("New peer list: ", newpeers)
                print("Contacting peer ", peer)

                try: 

                    status, res = self.network.register_self_to_peer(peer, 
                    self.host, self.port, self.netid)

                    if status == 200 and 'netid' in res:
                        if not isinstance(res, dict):
                            print("Register peer: Unexpected return type: ", res)
                            return

                        # Confirm network ID
                        if res['netid'] == self.netid or self.netid is None:
                            # Move peer to our own peer list

                            #print("Adding new peer ", peer)
                            self.storage.add_peer(peer)

                            # Add peers to newpeers
                            self.add_peers_to_set(newpeers, res['peers'])

                            # Subtract away our current peers
                            peer_set = self.storage.get_peer_set()
                            newpeers = [x for x in newpeers if x not in peer_set]
                            print("New peer set: ", newpeers)

                        else:
                            print("Bed network ID.")
                    else:
                        print("Bad peer download from peer ", peer)
                        print("Result: ", res)
                except Exception as e:
                    print("Connecting to peer failed: ", e)
                    raise
        
        #print("\n\nBEFORE PEER: ", self.storage.peers, "--\n\n")
        self.download_longest_chain(seed_download = seed_download)
        #print("\n\nEND PEER: ", self.storage.peers, "--\n\n")


    def copychain(self, copypeer, start = -1):
        """ Copies the chain from copypeer. If start = -1 we start
        from the end of the current chain. Otherwise we start from the specified block
        """

        # Download and install the longest chain

        if start == -1:
            start_index = self.storage.get_blockchain_len() - 1
        else:
            start_index = start

        print("Copy chain from peer: ", copypeer, " startingfrom index ", start_index)

        if start_index < 0:
            start_index = 0

        print("Downloading chain from index %d." % start_index)

        chain = self.network.download_chain(copypeer, start_index)
        unconfirmed_transactions = self.network.download_unconfirmed_transactions(copypeer)
        confirmed_transactions = self.network.download_confirmed_transactions(copypeer, start_index)

        self.add_chain_from_peer(chain, unconfirmed_transactions, confirmed_transactions)

    def add_chain_from_peer(self, chain, unconfirmed_transactions,
    confirmed_transactions):
        """ Attach the chain from the peer to our own chain
        """

        # We +1 because of genesis block 
        # The transactions always match up with one block later.

        if len(chain) != len(confirmed_transactions) + 1:
            print("Chain length does not match confirmed transaction length.")
            print("Length of chain: %d." % len(chain))
            print("Length of transactions: %d." % len(confirmed_transactions))
            return "Error: Chain/Transaction Mismatch"

        # Sort the confirmed_transaction and chain by blocknum / index
        sorted_confirmed_transactions = sorted(confirmed_transactions,
        key = lambda k: k['blocknum'])
        sorted_chain = sorted(chain, key = lambda k: k['index'])
        
        # Clear the proposed chain
        self.storage.clear_proposal_chain()

        # Now these should match up.
        for i, block in enumerate(chain):

            if i > 0:
                prev_block = self.storage.get_last_proposed_block()
            else:
                if block['index'] == 0:
                    prev_block = None
                else:
                    prev_block = self.storage.get_nth_block(block['index'] - 1)
            
            # Our transaction list is offset by 1 because
            # of the gensis block. Need to subtract


            if i>0:
                trans = confirmed_transactions[i-1]
            else:
                trans = {"blocknum":block['index'], "transactions":[]}

            if block['index'] > 0 and trans['blocknum'] != block['index']:
                print("Mismatched block/transaction number.")
                print("Block: %d, Trans: %d." % (block['index'], trans['blocknum']))
                return "Mismatched block/transaction"

            in_block = {"block": block, "trans_list":trans['transactions']}

            newblock = self.verify_block(in_block, prev_block)

            if newblock is not None:
                print("Adding new proposed block.")
                self.storage.add_block_to_proposal(newblock)

                print("Performing action.")
                self.common_mined_block_action(newblock, trans['transactions'])
            else:
                print("Bad block.")
                return "Bad block"
       
            print("Success! Replacing block.")

        print("Proposal chain: ", end='')

        for block in self.storage.proposal:
            print(block.index, " ", end = '')

        print()
        self.storage.replace_chain_with_proposal(confirmed_transactions,
        unconfirmed_transactions)

        # Confirm the transactions
        self.confirm_blocks()

    def get_coin(self):
        """ Returns the coin object """

        return self.coin

