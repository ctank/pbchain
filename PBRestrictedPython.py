import RestrictedPython
import time

safe_builtins = dict(RestrictedPython.safe_builtins)
safe_builtins['__name__'] = '__main__'
safe_builtins['time'] = time
safe_builtins['print'] = print

