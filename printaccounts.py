from pymongo import MongoClient

client = MongoClient('mongodb://localhost:27017')
db = client['accounts']

balances = db['balances']
confirmed_transactions = db['confirmed_transactions']

print("\nBALANCES:\n\n")

bals = balances.find().sort("ID")

for bal in bals:
    print("ID: %s\tBalance: %3.2f" % (bal['ID'], bal['balance']))

print("\nConfirmed Transactions:\n\n")
ctrans = confirmed_transactions.find().sort("ID")

for ctran in ctrans:
    print("ID: %s\tDebits: [", end = '')
    for d in ctran['debit']:
        print(d['amount'], ' ', end = '')

    print('] Credits: [', end = '')
    for c in ctran['credit']:
        print(c['amount'], ' ', end = '')

    print(']\tNett: %3.2f.' % ctran['nett'])

print("\n")

