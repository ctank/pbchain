##
## Object Types
##

COIN_OBJECT     = 0  # Coin operations
AGENT_OBJECT    = 1  # Agent objects
USER_OBJECT     = 2  # User (non-agent) objects:

### Commands

MINT_COIN       = 0
PAY_COIN        = 1

CREATE_VAR      = 999
UPDATE_VAR      = 998
CREATE_AGENT    = 997

### Phases

# Unconfirmed: transaction first created
# Mined: Transaction mined
# Confirmed: Transaction confirmed (further N blocks mined on top of the block containing
# this transaction)
UNCONFIRMED_PHASE   = 500
MINED_PHASE         = 499
CONFIRMED_PHASE     = 498
