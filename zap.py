from pymongo import MongoClient

client = MongoClient('mongodb://localhost:27017')

print("Deleting pb1.")
client.drop_database('pb1')
print("Deleting pb2.")
client.drop_database('pb2')
print("Deleting pb3.")
client.drop_database('pb3')
print("Deleting pb4.")
client.drop_database('pb4')

