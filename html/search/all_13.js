var searchData=
[
  ['verify_5fblock_172',['verify_block',['../classblockchain_1_1_blockchain.html#a2662c63ab3872d1d0282b4c54fb0250b',1,'blockchain::Blockchain']]],
  ['verify_5fmerkle_5ftree_173',['verify_merkle_tree',['../classblockchain_1_1_blockchain.html#a696a24476d90d2998d4cacb0ece800fd',1,'blockchain::Blockchain']]],
  ['verify_5fpbcoin_5ftransaction_174',['verify_pbcoin_transaction',['../classblockchain_1_1_blockchain.html#a6de9ae01203920a73b485c43a2062705',1,'blockchain::Blockchain']]],
  ['verify_5fproof_5fof_5fwork_175',['verify_proof_of_work',['../classblockchain_1_1_blockchain.html#aeaf5fcd4b8f5f3eeb1edd7d7b7519acc',1,'blockchain::Blockchain']]],
  ['verify_5fsignature_176',['verify_signature',['../namespacechain__utils.html#a17354735377dd64a5bb485ac6eb9d191',1,'chain_utils']]],
  ['verify_5ftransaction_177',['verify_transaction',['../classblockchain_1_1_blockchain.html#ab5aa5f133cb1a0747d8651277d2b16eb',1,'blockchain.Blockchain.verify_transaction()'],['../namespacechain__utils.html#a1917fbff9be12f7f61d63fa3e01a1632',1,'chain_utils.verify_transaction()']]],
  ['verify_5fwallet_5fid_178',['verify_wallet_ID',['../namespacechain__utils.html#adc85ff12d366e5addb70b896ba32c708',1,'chain_utils']]]
];
