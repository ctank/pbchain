""" This is the storage class for the blockchain, providing abstraction from the storage layer. 
There is an analogous network class for abstracting the network layer. By default this class
implements a simple list for blocks, unconfirmed and confirmed transactions. Override to
implement other mechanisms like databases, flat files, etc. Design philosophy is that
this class only performs storage. It WILL NOT manipulate the block or transactions in any way.
All of this is left to the blockchain.
"""

# for storage
import shelve
import os.path
import socket

class Storage:
    """
    ------------------- INTERNAL OVERRIDABLE METHODS ---------------------

    These are the methods that we will override for specific implementations 
    of the storage class. The default implementation stores everything as
    Python lists

    ------------------- INTERNAL OVERRIDABLE METHODS ---------------------
    """

    #
    # General Storage Management
    #

    def clear_storage(self):
        """ Clears the storage """
        self.chain1 = []
        self.chain2 = []

        # Proposal blockchain when handling chains from other peers
        self.unconfirmed_transactions = []
        self.confirmed_transactions = []

    def initialize_storage(self, dbname, connectString):
        """ Connects to database and clears it """
        self.clear_storage()

    #
    # Chain Management
    #

    def get_chain(self, chain_num, index = -1):
        """ Get a specified chain from the storage"""
        if chain_num == 1:
            ret =  self.chain1
        else:
            ret =  self.chain2

        if index >= 0:
            return ret[index:]
        else:
            return ret

    def get_chain_index(self, chain_num, index):
        """ Returns chain from position index onwards
        """

        chain = self.get_chain(chain_num)
        
        if len(chain) > index:
            return chain[index:]
        else:
            return chain

    def clear_chain(self, chain_num):
        """ Clears a specified chain from the storage"""
        if chain_num == 1:
            self.chain1 = []
        else:
            self.chain2 = []

        self.save_storage()

    def add_to_chain(self, chain_num, block):
        """ Add to the specified chain """

        chain = self.get_chain(chain_num)
        chain.append(block)
        self.save_storage()

    def copy_chain(self, from_chain_num, ancestor_hash):
        """ Copy from the from_chain to the other chain """

        # Note: clear_chain must be caled before get_chain as
        # it creates a completely new list and breaks the alias!

        if from_chain_num == 1:
            from_chain = self.get_chain(1)
            self.clear_chain(2)
            to_chain = self.get_chain(2)
        else:
            from_chain_num = self.get_chain(2)
            self.clear_chain(1)
            to_chain = self.get_chain(1)

        for block in from_chain:
            to_chain.append(block)
            if block.hash == ancestor_hash:
                break

        self.save_storage()
        assert(len(self.chain1) >= 1)
        assert(len(self.chain2) >= 1)

    def get_chain_len(self, chain_num):
        """ Get the length of a specific chain """
        chain = self.get_chain(chain_num)
        return len(chain)

    def get_nth_block_from_end(self, N):
        """ Fet the nth block from the end """
        chain = self.get_blockchain()

        if len(chain) >= N:
            return chain[-N]
        else:
            return None

    def get_last_block_from_chain(self, chain_num):
        """ Get the last block from a specified chain"""
        chain = self.get_chain(chain_num)
        return chain[-1]

    def get_genesis(self):
        chain = self.get_blockchain()

        if len(chain) > 0:
            return chain[0]
        else:
            return None

    def get_nth_block(self, N):
        chain = self.get_blockchain()

        if len(chain) <= N:
            return None
        else:
            return chain[N]

    def replace_chain(self, confirmed_transactions, unconfirmed_transactions):
        
        if len(self.proposal) > 0:
            first = self.proposal[0]

            if first.index == 0:
                # This is all the way from the Genesis Block!
                # Flush the blockchain and replace the entire chain

                self.flush_block_chain()
                self.chain1 = list(self.proposal)
                self.confirmed_transactions = list(confirmed_transactions)
                self.unconfirmed_transactions = list(unconfirmed_transactions)
            else:
                chain_num = self.get_blockchain_num()

                if chain_num == 1:
                    self.chain1[first.index:] = self.proposal
                else:
                    self.chain2[first.index:] = self.proposal

            self.proposal = []
            self.save_storage()
    #
    # Transaction Management
    #

    def get_trans_list(self, which):
        """ Get a specified transaction list.

        Params:
            which: 0 = Unconfirmed, 1 = Confirmed.
        """

        if which == 0:
            return self.unconfirmed_transactions
        else:
            return self.confirmed_transactions

    def get_trans_list_from_index(self, which, ndx):
        """ Get the specified transaction list from
        the index specified onwards:
        """

        trans_list = self.get_trans_list(which)

        return trans_list[ndx:]

    def get_trans_list_len(self, which):
        """ Get length of a  transaction list.

        Params:
            which: 0 = Unconfirmed, 1 = Confirmed.
        """
        return len(self.get_trans_list(which))

    def wipe_transactions(self, which):
        """ Wipe out transactions. 
            Params:
                which: Which set to wipe. 0 = Unconfirmed, 1 = Confirmed
        """
        if which == 0:
            self.unconfirmed_transactions = []
        else:
            self.confirmed_transactions = []

        self.save_storage()

    def add_transaction(self, which, trans, add_front = False):
        """ Add to specified transaction list.
        Params:
            which: 0 to add to unconfirmed, 1 to add to confirmed.
            trans: Transaction to add
        """

        trans_list = self.get_trans_list(which)

        if not add_front:
            trans_list.append(trans)
        else:
            trans_list.insert(0, trans)

        self.save_storage()

    def copy_transactions(self, blocknum, trans_list, target_list):
        target_list.append({"blocknum":blocknum, "transactions":trans_list})
        self.save_storage()

    def move_to_confirmed(self, blocknum):
        """ Move transactions from the unconfirmed list
        to the confirmed list
        """

        from_list = self.get_trans_list(0)
        to_list = self.get_trans_list(1)

        self.copy_transactions(blocknum, from_list, to_list)
        self.wipe_transactions(0)
        self.save_storage()

    def add_to_confirmed(self, blocknum, trans_list):
        to_list = self.get_trans_list(1)
        self.copy_transactions(blocknum, trans_list, to_list)

    
    def remove_from_unconfirmed(self, trans_list):
        """ Remove all transactions in trans_list from
        unconfirmed. Used to remove transactions in mined blocks
        """

        self.unconfirmed_transactions = [x for x in self.unconfirmed_transactions if x not in trans_list]

        self.save_storage()

    def check_in_unconfirmed(self, trans):
        """ Check if trans is already in the unconfirmed list
        """

        return trans in self.unconfirmed_transactions

    #
    # Save and restore functions. These may be
    # overriden if you are writing to flat files.
    # If you are using databases, override both
    # save_storage and load_storage and add a single
    # pass statement so that these functions don't do
    # anything, as they are called by mined_block_action.
    #

    def save_storage(self):
        """ Save the storage. The filename is specified in self.dbname.
        Override this with a single "pass" statement if you are
        using databses.
        """

        shelf = shelve.open(self.dbname)

        # Save blockchain state
        shelf['chain1'] = self.chain1
        shelf['chain2'] = self.chain2
        shelf['unconfirmed'] = self.unconfirmed_transactions
        shelf['confirmed'] = self.confirmed_transactions
        shelf['peers'] = self.peers

        # Save the storage state
        sstate = {'fork':self.fork, 'chain_num':self.chain_num, 'local_fork':self.local_fork,
        'last_confirmed':self.last_confirmed}
        shelf['state'] = sstate

        shelf.close()

    def load_storage(self):
        """ Load the storage. The filename is specified in self.dbname.
        Override this with a single "pass" statement if you are
        using databses.
        """

        #print("\nLoading blockchain from %s." % self.dbname)
        shelf = shelve.open(self.dbname)

        # Load the blockchain state
        self.chain1 = shelf['chain1']
        self.chain2 = shelf['chain2']
        self.unconfirmed_transactions = shelf['unconfirmed']
        self.confirmed_transactions = shelf['confirmed']
        self.peers = shelf['peers']

        # Load the storage state
        sstate = shelf['state']

        self.fork = sstate['fork']
        self.chain_num = sstate['chain_num']
        self.local_fork = sstate['local_fork']
        self.last_confirmed = sstate['last_confirmed']

        print("RESTORED LAST CONFIRMED TO %d." % self.last_confirmed)
        shelf.close()

    def restore_blockchain(self):
        if os.path.isfile("./" + self.dbname):
            self.load_storage()
            return True
        else:
            return False

    #
    # Peer Management
    #

    def set_seedpeer(self, seedpeer):
        self.seedpeer = seedpeer

    def test_seedpeer(self, peer):
        """ Checks if peer is a seed peer """

        if self.seedpeer is None:
            print("STORAGE: NO SEEDPEER SET.")
            return False

        thehost = socket.gethostbyname(peer['host'])

        #print("host: ", thehost == self.seedpeer['host'])
        #print("port: ", peer['port'] == self.seedpeer['port'])

        return thehost == self.seedpeer['host'] and self.seedpeer['port'] == peer['port']


    def check_self_address(self, peer):
        """ Ensure that we don't register ourselves """

        return peer['host'] == self.host and peer['port'] == self.port


    def test_duplicate(self, peer):
        for dup in self.peers:

            if dup['host'] == peer['host'] and dup['port'] == peer['port']:
                return True

        return False

    def add_peer(self, peer, no_netcheck = False):
        """ Add a new peer. Peer is a dictionary of the form:
            {'host':<host name>,
            'port':<port num>,
            'netid':<netid>
            }
        """

        if 'host' not in peer or 'port' not in peer or 'netid' not in peer:
            print("MALFORMED PEER PACKET.")
            return {"result":"Bad packet."}

        # We process if either we ar enot checking the netid, or the netid matches, or is None, or the
        # peer is a seed peer.
        if no_netcheck or peer['netid'] == self.get_netid() or peer['netid'] is None or self.test_seedpeer(peer):

            chainlen = self.get_blockchain_len()
            netid = self.get_netid()
            mypeers = self.get_peers()

            # Convert host name to IP address
            peer['host'] = socket.gethostbyname(peer['host'])

            if self.check_self_address(peer):
                print("CANNOT REGISTER SELF!")
                ret = {}

            if self.test_duplicate(peer):
                print("CANNOT ADD DUPLICATE!")
                ret = {"chainlen":chainlen, "netid":netid, "peers":mypeers}
            else:

                #print("Registering peer: ", peer)

                self.peers.append(peer)

                self.save_storage()
                ret = {"chainlen":chainlen, "netid":netid, "peers":mypeers}
        else:
            print("\n\nBAD NETWORK\n\n")
            print("Peer NETID: %s" % peer['netid'])
            ret = {"result":"Bad network ID."}

        return ret

    def get_peer_set(self):
        return self.peers

    def get_peers(self):
        return self.peers

    """
    -------------------- EXTERNAL FACING NON_OVERRIDE METHODS -------------------------------

    These are methods used by blockchain.py. 
    DO NOT OVERRIDE THESE

    -------------------- EXTERNAL FACING NON_OVERRIDE METHODS -------------------------------
    """


    def flush_block_chain(self):
        """ Flushes the entire blockchain and resets its state """

        self.clear_storage()
        self.fork = False
        self.chain_num = 1
        self.local_fork = 1
        self.loaded = False

    def __init__(self, dbname = 'pbchain.dat', connectString = '', M = 3):    
        """ Initializes the storage class.

        dbname = Name of database to store in. If you are using
            flat files this will contain the filename.
        connectString = Database connection string. E.g. mongodb://localhost:27017
            Leave empty if using flat files
        M = Minimum length difference before declaring one fork the winner

        Returns: Nil
        """

        self.dbname = dbname
        self.connectString = connectString

        self.M = M
        self.last_confirmed = 0

        # Check if the blockchain datafile exists. If so load it.
        # Otherwise flush it.

        self.seedpeer = None
        self.loaded = self.restore_blockchain()
        self.host = ''
        self.port = 999

        if not self.loaded:
            print("Flushing blockchain.")
            self.flush_block_chain()
            self.peers = []
            self.proposal = []
        else:
            print("Blockchain restored.")

    def start_fork(self, ancestor_hash):
        """ Starts the fork protocol. Called when
        a fork is detected. The anestor_hash is the common
        ancestor that is the point of the fork
        """

        self.copy_chain(self.chain_num, ancestor_hash)

        # Flip the local fork
        if self.local_fork == 1:
            self.local_fork = 2
        else:
            self.local_fork = 1

        self.fork = True
        self.save_storage()

    # We only choose a fork when the difference in length
    # is more than N blocks
    def resolve_fork(self):
        """ Attempt to resolve the fork. If one chain is longer
        than the other by M blocks (default M = 3), that chain
        is chosen to be the actual chain and the other is discarded.
        """

        chain1len = self.get_chain_len(1)
        chain2len = self.get_chain_len(2)

        if chain1len - chain2len >= self.M:
            self.chain = self.get_chain(1)
            self.chain_num = 1
            self.clear_chain(2)
            self.fork = False
            self.save_storage()
            return True
        elif chain2len - chain1len >= self.M:
            self.chain = self.get_chain(2)
            self.clear_chain(1)
            self.chain_num = 2
            self.fork = False
            self.save_storage()
            return True
        else:
            return False

    def get_last_block(self):
        """ Returns the last block on the blockchain. There should be at least one block, which
        is the genesis block.

            Params: None
            Returns: Last block in chain. Genesis block if chain is empty.
        """

        #print("GET LAST BLOCK")

        if not self.fork:
            return self.get_last_block_from_chain(self.chain_num)
        else:
            # Fork situation: If both chains are the
            # same length, choose accordingto local_fork
            # which is flipped each time there is a fork
            # Otherwise return the last block from the longer
            # fork.

            chain1len = get_chain_len(1)
            chain2len = get_chain_len(2)

            if chain1len == chain2len:
                if self.local_fork == 1:
                    return self.get_last_block_from_chain(1)
                else:
                    return self.get_last_block_from_chain(2)
            elif chain1len > chain2len:
                return self.get_last_block_from_chain(1)
            else:
                return self.get_last_block_from_chain(2)

    def add_genesis_block(self, block, force = False):
        """ Adds the genesis block.

            Params: 
                block: The genesis block. Chain is erased.
            Returns: Length of chain. Always 1.
        """

        # Note: We can't just simply do self.chain = [block] as this
        # will just create a new list and assign it to chain, as opposed
        # to correctly updating the list it is aliased to.

        #print("GENESIS self.loaded: ", self.loaded, " force: ", force)
        if (not self.loaded) or force:
            print("Creating new blockchain")
            self.clear_chain(self.chain_num)
            self.add_to_chain(self.chain_num, block)
            self.save_storage()
            return 1
        else:
            print("Reusing old blockchain")
            return -1

    def add_block_to_alt_chain(self, block):
        """ Add a new block to the alternate chain, when we have a fork.
        """

        if self.chain_num == 1:
            self.add_to_chain(2, block)
            self.save_storage()
            return self.get_chain_len(2)
        else:
            self.add_to_chain(1, block)
            self.save_storage()
            return self.get_chain_len(1)

    def add_block_to_curr_chain(self, block):
        self.add_to_chain(self.chain_num, block)
        self.save_storage()
        return self.get_chain_len(self.chain_num)

    def detect_fork(self, block):
        """ Detects if there's a fork

        Returns 3 values:
            0: No fork
            1: fork
            2: Orphan or unknown block
            3: Replay block
            4: Block too old and discarded
        """

        prev = self.get_last_block_from_chain(self.chain_num)

        if block.prev_hash != prev.hash:
            if block.prev_hash == prev.prev_hash:
                if block.hash != prev.hash:
                    if abs(block.time_stamp - prev.time_stamp) < 60:
                        # They share a common ancestor block. So this is a fork
                        return 1
                    else:
                        # Expired block
                        return 4
                else:
                    # The hashes match with a previous block, so this is a replay
                    return 3
            else:
                # Don't know what this is!
                return 2
        else:
            # There's no fork
            return 0

    def forked_add(self, block):
        """ In a fork situation, we add the block
        to the chain that has a matching last block
        """
        print("FORK!")
        last_1 = self.get_last_block_from_chain(1)
        last_2 = self.get_last_block_from_chain(2)

        if block.prev_hash == last_1.hash:
            self.add_to_chain(1, block)
            self.save_storage()
            retlen = self.get_chain_len(1)
        elif block.prev_hash == last_2.hash:
            self.add_to_chain(2, block)
            self.save_storage()
            retlen = self.get_chain_len(2)
        else:
            # Discard the block
            print("BLOCK DISCARDED")
            retlen = -1

        return etlen

    def add_block_to_chain(self, block):
        """ Add a new block to the chain.

            Params:
                block: Block to be added.
            Returns:
                Length of chain after block is added.
        """
        
        if not self.fork:

            # Test if there's a fork. If so, start the fork process 
            # and append to the chain that is NOT the current chain
        
            fork_type = self.detect_fork(block)

            if fork_type == 0:
                # Straightforward: Add the block
                retlen =  self.add_block_to_curr_chain(block)
            elif fork_type == 1:
                # We start the fork process
                print("FORK DETECTED.")
                self.start_fork(block.prev_hash)

                # Add block to the OTHER chain
                retlen =  self.add_block_to_alt_chain(block)
            elif fork_type == 2:
                print("\nUNKNOWN BLOCK ORIGIN\n")
                retlen = -1
            elif fork_type == 4:
                print("\nEXPIRED BLOCK. DISCARDING\n")
                retlen = -1
            else:
                print("\nBLOCK REPLAY\n")
                retlen = -1
        else:
            # There is a fork; find which chain to append to.
            retlen = self.forked_add(block)

            # See if we can choose one fork.
            print("RESOLVING FORK.")
            if self.resolve_fork():
                print("FORK RESOLVED.")
                self.save_storage()
            else:
                print("FORK STILL UNRESOLVED. CHAIN 1 LENGTH: %d, CHAIN 2 LENGTH: %d." % (len(self.chain1), len(self.chain2)))

            print("ADD BLOCK RETURNING %d." % retlen)
        return retlen

    def clear_unconfirmed_transactions(self):
        """ Clears all unconfirmed transactions.
        This clears ALL unconfirmed transactions without checking. Use with caution!

            Params: None
            Returns: None
        """

        # 0 = Unconfirmed transactions
        self.wipe_transactions(0)

    def add_mint_to_unconfirmed(self, mint_trans):
        """ Add mint transaction to fromt """
        
        self.add_transaction(0, mint_trans, add_front = True)

    def add_trans_to_unconfirmed(self, trans):
        """ Add a new transaction to unconfirmed list.

            Params:
                trans: Transaction to add.

            Returns:
                # of unconfirmed transactions after adding.
        """

        self.add_transaction(0, trans)
        self.save_storage()
        return self.get_trans_list_len(0)

    def get_unconfirmed_transactions_count(self):
        return len(self.get_trans_list(0))

    def get_unconfirmed_transactions(self):
        """ Get list of unconfirmed transactions.

            Params: None
            Returns: 
                List of unconfirmed transactions.
        """

        return self.get_trans_list(0)

    def wipe_unconfirmed(self):
        self.wipe_transactions(0)

    def add_trans_to_confirmed(self, trans):
        """ Add transaction to list of confirmed transactions
            Params: None
            Returns:
                # of confirmed transactions after adding.
        """

        self.add_transaction(1, trans)
        self.save_storage()
 
    def move_trans_to_confirmed(self, index):
        self.move_to_confirmed(index)

        # Save the state
        self.save_storage()

    def get_blockchain_num(self):

        if not self.fork:
            return self.chain_num
        else:
            len1 = self.get_chain_len(1)
            len2 = se;f.get_chain_len(2)

            if len1 > len2:
                return 1
            elif len2 > len1:
                return 2
            else:
                return self.local_fork
               

    def get_blockchain(self, start = 0):
        """ Return the blockchain
        """

        chain_num = self.get_blockchain_num()
        return self.get_chain(chain_num, start)


    def get_blockchain_len(self):
        chain = self.get_blockchain(0)
        return len(chain)

    def get_unconfirmed_transactions(self):
        return self.get_trans_list(0)

    def get_confirmed_transactions(self, index):
        return self.get_trans_list_from_index(1, index)

    def get_netid(self):
        genesis = self.get_genesis()

        if genesis is None:
            return None

        else:
            return genesis.hash

    # Support for a proposal chain
    def clear_proposal_chain(self):
        self.proposal = []

    def add_block_to_proposal(self, block):
        self.proposal.append(block)

    def get_last_proposed_block(self):
        if len(self.proposal) < 1:
            return None
        else:
            return self.proposal[-1]

    def replace_chain_with_proposal(self, confirmed_transactions, unconfirmed_transactions):
        self.replace_chain(confirmed_transactions, unconfirmed_transactions) 

    def register_seedpeer(self, seedpeer):

        seedpeer['host'] = socket.gethostbyname(seedpeer['host'])

        self.seedpeer = seedpeer
        #self.add_peer(seedpeer)

    def set_self_info(self, host, port):
        """ Set my own server info """
        self.host = socket.gethostbyname(host)
        self.port = port

    # Managing confirmations
    def set_last_confirmed(self, last_confirmed):
        """ Set the last block confirmed
        """

        print("LAST CONFIRMED WAS %d." % self.last_confirmed)
        self.last_confirmed = last_confirmed

        print("LAST CONFIRM IS NOW %d." % self.last_confirmed)

        self.save_storage()


    def get_last_confirmed(self):
        """ Get the last confirmed block
        """

        return self.last_confirmed
